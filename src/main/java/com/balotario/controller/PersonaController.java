package com.balotario.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.balotario.api.ApiOutResponse;
import com.balotario.api.ExisteVerificarRequest;
import com.balotario.bus.PersonaBus;
import com.balotario.entity.Persona;
import com.balotario.util.Constantes;

import io.swagger.annotations.Api;


@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/persona")
@Api(value = "persona")
public class PersonaController {
	
	@Autowired
	private PersonaBus personaBus;

	@GetMapping("/listarTipoDocumento")
	public ApiOutResponse listarTipoDocumento() {
		return personaBus.listarTipoDocumento();
	}
	
	@GetMapping("/listarCategoria")
	public ApiOutResponse listarCategoria() {
		return personaBus.listarCategoria();
	}
	
	@GetMapping("/listarCargo")
	public ApiOutResponse listarCargo() {
		return personaBus.listarCargo();
	}
	
	@GetMapping("/listarRol")
	public ApiOutResponse listarRol() {
		return personaBus.listarRol();
	}
	
	@GetMapping("/listarArea/{idCategoria}")
	public ApiOutResponse listarArea(@PathVariable int  idCategoria) {
		return personaBus.listarArea(idCategoria);
	}
	
	@GetMapping("/listarLocal/{idCategoria}/{idArea}")
	public ApiOutResponse listarLocal(@PathVariable int  idCategoria, @PathVariable int  idArea) {
		return personaBus.listarLocal(idCategoria, idArea);
	}
	
	@GetMapping("/listarPersonas")
	public ApiOutResponse listarPersonas() {
		return personaBus.listadoPersona(1);
	}
	
	@PostMapping("/buscarPersonas")
	public ApiOutResponse buscarPersonas(@RequestBody String textoBuscar) {
		return personaBus.buscarPersona(textoBuscar);
	}
	
	@PostMapping("/verificarUsuario")
	public ApiOutResponse verificarUsuario(@RequestBody String usuario) {
		return personaBus.usuarioExiste(usuario);
	}
	
	@PostMapping("/verificarCodigoTrabajador")
	public ApiOutResponse verificarCodigoTrabajador(@RequestBody ExisteVerificarRequest verificarRequest) {
		return personaBus.codTrabajadorExiste(verificarRequest.getCodUsuario(), verificarRequest.getCodTrabajador());
	}
	
	@PostMapping("/verificarNumeroDocumento")
	public ApiOutResponse verificarNumeroDocumento(@RequestBody ExisteVerificarRequest verificarRequest) {
		return personaBus.numDocumentoExiste(verificarRequest.getCodUsuario() , verificarRequest.getNumDocumento());
	}
	
	@PostMapping("/registrarPersona")
	public ResponseEntity<?> crear(@RequestBody Persona persona){
		
		Map<String, Object> response = new HashMap<>();

		try {
			
			personaBus.registrarUsuario(persona);
			
		} catch(DataAccessException e) {
			
			response.put(Constantes.MENSAJE, "Error al realizar insert en la base de datos!");
			response.put(Constantes.ERROR, e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put(Constantes.MENSAJE, "La persona fue creado con exito!");

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
				
	} 
	
	
	
	@GetMapping("/obtenerPersona/{codUsuario}")
	public ApiOutResponse obtenerPersona(@PathVariable String codUsuario) {
		return personaBus.obtenerPersona(codUsuario);
	}
	
	
	@PostMapping("/actualizarPersona")
	public ResponseEntity<?> actualizarPersona(@RequestBody Persona persona){
		
		Map<String, Object> response = new HashMap<>();

		try {
			
			personaBus.actualizarPersona(persona);
			
		} catch(DataAccessException e) {
			
			response.put(Constantes.MENSAJE, "Error al realizar actualizar en la base de datos!");
			response.put(Constantes.ERROR, e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		response.put(Constantes.MENSAJE, "La persona fue actualizada con exito!");

		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
				
	} 
}

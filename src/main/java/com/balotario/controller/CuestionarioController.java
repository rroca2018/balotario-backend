package com.balotario.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.balotario.bus.CuestionarioBus;
import com.balotario.api.ApiOutResponse;

import io.swagger.annotations.Api;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/cuestionario")
@Api(value = "Cuestionario")
public class CuestionarioController {

	@Autowired
	private CuestionarioBus cuestionarioBus;

	@GetMapping("/listaPregunta/{cantidadPregunta}/{idBalotario}")
	public ApiOutResponse listaPregunta(
			@PathVariable(name = "cantidadPregunta", required = true) Long cantidadPregunta,
			@PathVariable(name = "idBalotario", required = true) Long idBalotario) {
		return cuestionarioBus.listaPregunta(cantidadPregunta,idBalotario);
	}

}

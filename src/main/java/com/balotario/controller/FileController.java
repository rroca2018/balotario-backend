package com.balotario.controller;

import java.nio.file.Files;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.balotario.api.ApiOutResponse;
import com.balotario.bus.FileBus;
import com.balotario.util.Util;

import io.swagger.annotations.Api;


@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
@RequestMapping("/archivo")
@Api(value = "archivo")
public class FileController {
	
	@Autowired
	FileBus fileBus;
	
	
	@Value("${rutaArchivoPlantilla}")
	private String path;
	
	@GetMapping("/descargarArchivo")
	public void descargarArchivo(HttpServletResponse res) throws Exception {
		System.out.println("ruta -> " + path);
		String file = Util.getObtenerArchivoDesdeRutaCarpeta(path);
		String ruta = path+file;
		String fileName = file;

		res.setHeader("Content-Disposition", "attachment; filename=" + fileName);
		res.getOutputStream().write(Files.readAllBytes(Paths.get(ruta)));
	}
	
	// LECTURA EXCEL PERSONAS
	@PostMapping("/validarCargaExcelPersonas")
	public ApiOutResponse validarCargaExcelPersonas(
			@RequestHeader(name = "idUsuario", required = false) Long idUsuario,
			@RequestParam("file") MultipartFile file, HttpServletRequest request) throws Exception {
		return fileBus.validarCargaExcelPersona(idUsuario, file, request);
	}
	
	
	

}

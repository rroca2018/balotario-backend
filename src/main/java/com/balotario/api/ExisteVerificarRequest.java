package com.balotario.api;

public class ExisteVerificarRequest {

	private int codUsuario;
	private String codTrabajador;
	private String numDocumento;
	
	
	public int getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(int codUsuario) {
		this.codUsuario = codUsuario;
	}
	public String getCodTrabajador() {
		return codTrabajador;
	}
	public void setCodTrabajador(String codTrabajador) {
		this.codTrabajador = codTrabajador;
	}
	public String getNumDocumento() {
		return numDocumento;
	}
	public void setNumDocumento(String numDocumento) {
		this.numDocumento = numDocumento;
	}
	
	
	
}

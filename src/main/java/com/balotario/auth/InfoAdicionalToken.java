/*
 * 
 */
package com.balotario.auth;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import com.balotario.bean.AuthUsuario;
import com.balotario.bus.AccesoBus;

@Component
public class InfoAdicionalToken implements TokenEnhancer {

	@Autowired
	private AccesoBus accesoBus;

	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {

		AuthUsuario usu = accesoBus.findByUsername(authentication.getName());

		Map<String, Object> info = new HashMap<>();

		info.put("id_usuario", usu.getIdCodigo());
		info.put("username", usu.getCidUsuario());
		info.put("nombres", usu.getCidNombre());
		info.put("puesto", usu.getPuesto());
		info.put("id_cargo",usu.getIdCargo());
		info.put("id_balotario", usu.getIdBalotario());
		info.put("cantidad_intento",usu.getCantidadIntento());
		info.put("cantidad_tiempo", usu.getCantidadTiempo());
		info.put("cantidad_pregunta", usu.getCantidadPregunta());

		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(info);

		return accessToken;
	}

}

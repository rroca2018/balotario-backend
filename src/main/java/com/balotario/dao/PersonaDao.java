package com.balotario.dao;

import java.util.List;

import com.balotario.api.ApiOutResponse;
import com.balotario.entity.Persona;

public interface PersonaDao {

	ApiOutResponse listarItems(String tipo, int idCategoria, int idArea);
	
	int grabarPersona(Persona persona);
	
	ApiOutResponse listadoPersona(int idEmpresa);
	
	ApiOutResponse obtenerPersona(String codUsuario);
	
	int actualizarPersona(Persona persona);
	
	int grabarPersonaMasivo(List<Persona> lstPersona);
	
	ApiOutResponse validarDataFormulario(String dato, Integer idUsuario, String valor);
	
	ApiOutResponse buscarPersona(String textoBuscar);
	
}

/*
 * 
 */
package com.balotario.dao;

import java.util.List;

import com.balotario.bean.AuthUsuario;
import com.balotario.bean.Menu;

public interface AccesoDao {

	public AuthUsuario autenticacionUsuario(String username);
	
	public List<Menu> menu(int idUsuario);
	

}

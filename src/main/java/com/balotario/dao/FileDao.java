package com.balotario.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;

import com.balotario.api.ApiOutResponse;
import com.balotario.bean.PersonaExcelAux;

public interface FileDao {

	ApiOutResponse validarCargaExcelPersona( Long idUsuario, MultipartFile  file, HttpServletRequest request);
	byte[] verTxtErrores(String nombreListado, HttpServletRequest request );
	
	ApiOutResponse buscarDato(String dato, String valor);
	
	HashMap<String, Object> encontrarRepetidos(List<PersonaExcelAux> lstPersonaExcelAux);
}

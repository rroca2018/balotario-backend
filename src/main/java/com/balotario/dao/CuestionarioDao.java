package com.balotario.dao;

import com.balotario.api.ApiOutResponse;

public interface CuestionarioDao {
	
	ApiOutResponse listaPregunta(Long cantidadPregunta,Long idBalotario);

}

package com.balotario.dto;



public class PersonaDTO {

	private int codEmpresa;
	private int codUsuario;
	private String nombreCompleto;
	private String noUsuario;
	private String correo;
	private int idCargo;
	private String desCargo;
	private int idArea;
	private String desArea;
	
	
	
	
	public int getCodUsuario() {
		return codUsuario;
	}
	public void setCodUsuario(int codUsuario) {
		this.codUsuario = codUsuario;
	}

	
	public String getNoUsuario() {
		return noUsuario;
	}
	public void setNoUsuario(String noUsuario) {
		this.noUsuario = noUsuario;
	}
	public int getCodEmpresa() {
		return codEmpresa;
	}
	public void setCodEmpresa(int codEmpresa) {
		this.codEmpresa = codEmpresa;
	}
	public String getNombreCompleto() {
		return nombreCompleto;
	}
	public void setNombreCompleto(String nombreCompleto) {
		this.nombreCompleto = nombreCompleto;
	}
	
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public int getIdCargo() {
		return idCargo;
	}
	public void setIdCargo(int idCargo) {
		this.idCargo = idCargo;
	}
	public String getDesCargo() {
		return desCargo;
	}
	public void setDesCargo(String desCargo) {
		this.desCargo = desCargo;
	}
	public int getIdArea() {
		return idArea;
	}
	public void setIdArea(int idArea) {
		this.idArea = idArea;
	}
	public String getDesArea() {
		return desArea;
	}
	public void setDesArea(String desArea) {
		this.desArea = desArea;
	}
	

	
}

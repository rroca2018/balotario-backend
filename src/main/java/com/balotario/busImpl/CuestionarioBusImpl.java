package com.balotario.busImpl;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.balotario.bus.CuestionarioBus;
import com.balotario.api.ApiOutResponse;
import com.balotario.dao.CuestionarioDao;

@Service
@Transactional(readOnly = true)
public class CuestionarioBusImpl implements CuestionarioBus {

	private final Log log = LogFactory.getLog(getClass());

	@Autowired
	private CuestionarioDao cuestionarioDao;

	ApiOutResponse mensajeExcepcion(ApiOutResponse rpta, Exception e, String metodo) {
		rpta.setCodResultado(500);
		rpta.setMsgResultado(e.toString());
		rpta.setTotal(0);
		rpta.setResponse(null);
		log.error("Ocurrio un error en " + metodo + "]: " + e.toString());
		return rpta;
	}

	@Override
	public ApiOutResponse listaPregunta(Long cantidadPregunta,Long idBalotario) {
		ApiOutResponse rpta = new ApiOutResponse();
		try {
			rpta = cuestionarioDao.listaPregunta(cantidadPregunta,idBalotario);
		} catch (Exception e) {
			mensajeExcepcion(rpta, e, "listar");
		}
		return rpta;
	}

}

package com.balotario.busImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.balotario.api.ApiOutResponse;
import com.balotario.bus.PersonaBus;
import com.balotario.dao.PersonaDao;
import com.balotario.entity.Persona;
import com.balotario.util.Constantes;
import com.balotario.util.LoggerCustom;


@Service
public class PersonaBusImpl implements PersonaBus {


	@Autowired
	private PersonaDao personaDao;


	@Override
	public ApiOutResponse listarTipoDocumento() {
		ApiOutResponse rpta = new ApiOutResponse();
		try {
			rpta = personaDao.listarItems(Constantes.TIPO_DOCUMENTO, 0, 0);
		}catch (Exception e) {
			LoggerCustom.mensajeExcepcion(rpta, e, "listar");
		}
		return rpta;
	}

	@Override
	public ApiOutResponse listarCategoria() {
		ApiOutResponse rpta = new ApiOutResponse();
		try {
			rpta = personaDao.listarItems(Constantes.CATEGORIA, 0, 0);
		}catch (Exception e) {
			LoggerCustom.mensajeExcepcion(rpta, e, "listar");
		}
		return rpta;
	}

	@Override
	public ApiOutResponse listarCargo() {
		ApiOutResponse rpta = new ApiOutResponse();
		try {
			rpta = personaDao.listarItems(Constantes.CARGO, 0, 0);
		}catch (Exception e) {
			LoggerCustom.mensajeExcepcion(rpta, e, "listar");
		}
		return rpta;
	}

	@Override
	public ApiOutResponse listarRol() {
		ApiOutResponse rpta = new ApiOutResponse();
		try {
			rpta = personaDao.listarItems(Constantes.ROL, 0, 0);
		}catch (Exception e) {
			LoggerCustom.mensajeExcepcion(rpta, e, "listar");
		}
		return rpta;
	}

	@Override
	public ApiOutResponse listarArea(int idCategoria) {
		ApiOutResponse rpta = new ApiOutResponse();
		try {
			rpta = personaDao.listarItems(Constantes.AREA, idCategoria, 0);
		}catch (Exception e) {
			LoggerCustom.mensajeExcepcion(rpta, e, "listar");
		}
		return rpta;
	}

	@Override
	public ApiOutResponse listarLocal(int idCategoria, int idArea) {
		ApiOutResponse rpta = new ApiOutResponse();
		try {
			rpta = personaDao.listarItems(Constantes.LOCAL, idCategoria, idArea);
		}catch (Exception e) {
			LoggerCustom.mensajeExcepcion(rpta, e, "listar");
		}
		return rpta;
	}

	@Override
	public int registrarUsuario(Persona persona) {
		persona.setIdEstado(1);
		int rpta = personaDao.grabarPersona(persona);
		return rpta;
	}

	@Override
	public ApiOutResponse listadoPersona(int idEmpresa) {
		ApiOutResponse rpta = new ApiOutResponse();
		try {
			rpta = personaDao.listadoPersona(idEmpresa);
		}catch (Exception e) {
			LoggerCustom.mensajeExcepcion(rpta, e, "listadoPersona");
		}
		return rpta;
	}

	@Override
	public ApiOutResponse obtenerPersona(String codUsuario) {
		ApiOutResponse rpta = new ApiOutResponse();
		try {
			rpta = personaDao.obtenerPersona(codUsuario);
		}catch (Exception e) {
			LoggerCustom.mensajeExcepcion(rpta, e, "obtenerPersona");
		}
		return rpta;
	}

	@Override
	public int actualizarPersona(Persona persona) {
		persona.setIdEstado(1);
		int rpta = personaDao.actualizarPersona(persona);
		return rpta;
	}

	@Override
	public ApiOutResponse usuarioExiste(String valor) {
		// TODO Auto-generated method stub
		return personaDao.validarDataFormulario(Constantes.VALOR_USUARIO, 0, valor);
	}

	@Override
	public ApiOutResponse codTrabajadorExiste(Integer idUsuario, String valor) {
		// TODO Auto-generated method stub
		return personaDao.validarDataFormulario(Constantes.VALOR_COD_TRAB, idUsuario, valor);
	}

	@Override
	public ApiOutResponse numDocumentoExiste(Integer idUsuario,  String valor) {
		// TODO Auto-generated method stub
		return  personaDao.validarDataFormulario(Constantes.VALOR_NUM_DOCU, idUsuario,  valor);
	}

	@Override
	public ApiOutResponse buscarPersona(String textoBuscar) {
		// TODO Auto-generated method stub
		return personaDao.buscarPersona(textoBuscar);
	}



}

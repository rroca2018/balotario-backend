package com.balotario.busImpl;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.impl.cookie.DateParseException;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.balotario.api.ApiOutResponse;
import com.balotario.bean.Excel;
import com.balotario.bean.PersonaExcelAux;
import com.balotario.bus.FileBus;
import com.balotario.dao.FileDao;
import com.balotario.dao.PersonaDao;
import com.balotario.entity.MensajeError;
import com.balotario.entity.Persona;
import com.balotario.util.Constantes;
import com.balotario.util.Util;



@Service
@Transactional(readOnly = true)
public class FileBusImpl  implements FileBus{
	
	
	@Autowired
	PersonaDao personaDao;
	
	@Autowired
	FileDao fileDao;
	
	private  final Log log = LogFactory.getLog(getClass());
	
	HttpSession sessionExcel = null;
	
	private static final String MSJ_ERROR_LEER_CARGA_EXCEL = "Error cargar y leer excel"; 
	private static final String MSJ_ERROR_LECTURA_EXCEL = "Error al leer el archivo excel"; 
	private static final String MSJ_ERROR_EXTENSION_ARCHIVO = "Error en la extension del archivo"; 
	private static final String MSJ_ERROR_SIN_ARCHIVO = "No hay un archivo cargado"; 
	private static final String GASTOS_SUPERVISION = "GASTOS DE SUPERVISION"; 
	
	private static final String MSJ_ERROR_NO_ENCUENTRA_ARCHIVO = "Error al leer archivo, no se encuentra el archivo"; 
	private static final String MSJ_ERROR_IO_ARCHIVO = "Error al leer archivo, error I/O archivo"; 
	private static final String MSJ_VERIFICAR_TXT_DESCARGADO = "ERROR, VERIFICAR EL ARCHIVO DE TEXTO DESCARGADO"; 

	@Override
	public ApiOutResponse validarCargaExcelPersona(Long idUsuario, MultipartFile file, HttpServletRequest request) {
		ApiOutResponse outResponse = new ApiOutResponse();
		String mimiTypeExcel =Constantes.MIME_APPLICATION_XLS;
		String mimiTypeExcelXlsx =Constantes.MIME_APPLICATION_XLSX;
		HttpSession session = request.getSession(true);
		/*DatoGeneral dato = new DatoGeneral();
		dato.setIdUsuario(idUsuario);
		dato.setIp(Util.getClientIp(request));
		dato.setIdMovimientoProyecto(idMovimientoProyecto);
		dato.setNombreArchivo(file.getOriginalFilename());*/
		
		if (file!=null &&  file.getSize()> 0){ 
			if(mimiTypeExcel.equals(file.getContentType()) || mimiTypeExcelXlsx.equals(file.getContentType())){
				try {
					MultipartFile uploaded = file;
					InputStream fileInputStream = uploaded.getInputStream();
						HashMap<String, Object> respuesta = leerDatosExcelPersona(request, fileInputStream, file);
						
						 String retorna = (String) respuesta.get("rpta"); 
						if (retorna.toUpperCase().equals("OK")) {
							
								outResponse.setMsgResultado("OK");
								outResponse.setCodResultado(1);
							
						}else if (retorna.equals("0")) {
							Excel objExcel = new Excel();
							String rowPos = respuesta.get("row").toString();
							String colPos = respuesta.get("col").toString();
							objExcel.setMessage((String) respuesta.get("message"));
							objExcel.setNroFila(Integer.parseInt(rowPos));
							objExcel.setNroColumna(colPos);
							outResponse.setCodResultado(0);
							outResponse.setMsgResultado(MSJ_VERIFICAR_TXT_DESCARGADO);	//EN FRONTEND RETORNA EL MENSJAE + ARCHIVO TXT 
							outResponse.setResponse(objExcel);
						}else{										
							outResponse.setCodResultado(2);		//EN FRONTEND RETORNA SOLO EL MENSJAE 
							outResponse.setMsgResultado("Error al cargar el archivo excel");	
						}
						
				} catch (Exception e) {
					log.error(MSJ_ERROR_LEER_CARGA_EXCEL+e);
					outResponse.setMsgResultado(MSJ_ERROR_LEER_CARGA_EXCEL);
					outResponse.setCodResultado(2);
				}			
			}else{
				log.info(MSJ_ERROR_EXTENSION_ARCHIVO);
				outResponse.setMsgResultado(MSJ_ERROR_EXTENSION_ARCHIVO);
				outResponse.setCodResultado(2);
			}		
		}else{
			log.info(MSJ_ERROR_SIN_ARCHIVO);
			outResponse.setMsgResultado(MSJ_ERROR_SIN_ARCHIVO);
			outResponse.setCodResultado(2);//no existe un archivo cargado
		}
		return outResponse;
	}

	private HashMap<String, Object> leerDatosExcelPersona(HttpServletRequest request, InputStream fileInputStream, MultipartFile file) {
		
		 HashMap<String, Object> map = new HashMap<>(); 
		sessionExcel = request.getSession(true);
		List<MensajeError> listaErrores =new ArrayList< MensajeError> ();
		Persona objPersona;
		List<Persona> lstPersona = new ArrayList<Persona>();
		List<PersonaExcelAux> lstPersonaExcelAux = new ArrayList<PersonaExcelAux>();
		String rpta = "";
		
		int rptaInsert = 0;

		Cell celEmpresa;
		Cell celUsuario;
		Cell celClave;
		Cell celCodTrabajador;
		Cell celPriNombre;
		Cell celSegNombre;
		Cell celApePaterno;
		Cell celApeMaterno;
		Cell celEmail;
		Cell celSexo;
		Cell celFecNacimiento;
		Cell celFecIngreso;
		Cell celSueBasico;
		Cell celTipDocumento;
		Cell celNumDocumento;
		Cell celCargo;
		Cell celCategoria;
		Cell celArea;
		Cell celLocal;
		Cell celRol;
		Cell celEstado;

		
		int codUsuario = 0;
		Integer codTipDoc = 0;
		Integer codCargo = 0;
		Integer codCategoria = 0;
		Integer codArea = 0;
		Integer codLocal = 0;
		Integer codRol = 0;
		int codEstado = 0;
		
		String empresa ="";
		String usuario ="";
		String clave ="";
		String codTrabajador ="";
		String priNombre ="";
		String segNombre ="";
		String apePaterno ="";
		String apeMaterno ="";
		String email ="";
		String sexo ="";
		String fecNacimiento ="";
		String fecIngreso ="";
		BigDecimal sueBasico =  BigDecimal.ZERO;
		String tipDocumento ="";
		String numDocumento ="";
		String cargo ="";
		String categoria ="";
		String area ="";
		String local ="";
		String rol ="";
		String estado ="";
		int flagAccion = 0;
		
		
		Workbook libro = null;
		
			try {
				libro = WorkbookFactory.create(fileInputStream);
		 //libro = new HSSFWorkbook(fileInput);// Se lee libro xlsx
			
			if (libro != null) {
				Sheet hojaUno = libro.getSheetAt(0);// 0 primera hoja del excel
				int numRows = hojaUno.getPhysicalNumberOfRows();
				System.out.println( hojaUno.getPhysicalNumberOfRows());		
				
				HSSFCell cell;
		
				for (int i = 1; i <= numRows; i++) {
					Row rowi = hojaUno.getRow(i);
						if (rowi != null) {
							//int d = rowi.getPhysicalNumberOfCells();
							int contador = 0;
							boolean filaExcelNulas = false; 
							for(int j=0; j<rowi.getLastCellNum(); j++) {
								cell = (HSSFCell) rowi.getCell(j, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
				
								if(cell.CELL_TYPE_BLANK == cell.getCellType()) {
									contador++;						
								}
								
								if(contador <= 2 && j == 5){
									break;
								}else if(contador > 5) {
									filaExcelNulas = true;
									break;
								}
								
							}
	
						if(filaExcelNulas == false) {
							// CELDA EMPRESA
							celEmpresa = rowi.getCell(0);
		
							if(celEmpresa!=null && Cell.CELL_TYPE_STRING == celEmpresa.getCellType()){
								try {
									empresa = celEmpresa.getStringCellValue();
									ApiOutResponse apiOutResponse = buscarDato(Constantes.EMPRESA, empresa);
									if(apiOutResponse.getCodResultado().equals(0)) {
										map.put("rpta",  Constantes.EXCEL_RESPUESTA_CERO);
										map.put("message", Constantes.EXCEL_EMPRESA_NO_EXISTE);
										map.put("row", i+1);
										map.put("col", 'A');
										break;
									}
									
									
								
								} catch (Exception e) {
									empresa=null;
								}	
							}else if(celEmpresa!=null && Cell.CELL_TYPE_NUMERIC == celEmpresa.getCellType()){
								empresa=String.valueOf(celEmpresa.getNumericCellValue());
							}else{
								 if(empresa.isEmpty()) {
										map.put("rpta", Constantes.EXCEL_RESPUESTA_CERO);
										map.put("message", Constantes.EXCEL_EMPRESA_OBLIGATORIA);
										map.put("row", i+1);
										map.put("col", 'A');
										break;
								   }
							}
							
						 
							
							// CELDA USUARIO
							celUsuario = rowi.getCell(1);
						
							if(celUsuario!=null && Cell.CELL_TYPE_STRING == celUsuario.getCellType()){
								try {
									usuario = celUsuario.toString();	
									
								} catch (Exception e) {
									usuario=null;
								}	
							}else if(celUsuario!=null && Cell.CELL_TYPE_NUMERIC == celUsuario.getCellType()){
								int usuarioAux = Integer.valueOf((int) celUsuario.getNumericCellValue());
								usuario = String.valueOf(usuarioAux);
								ApiOutResponse apiOutResponse = buscarDato(Constantes.USUARIO, usuario);
								if(apiOutResponse.getCodResultado() == 1) {
									codUsuario = Integer.parseInt(apiOutResponse.getResponse().toString());
									flagAccion = 1;
								}else {
									codUsuario = 0;
									flagAccion = 0;
								}

						   }else{
							   if(usuario.isEmpty()) {
									map.put("rpta", Constantes.EXCEL_RESPUESTA_CERO);
									map.put("message", Constantes.EXCEL_USUARIO_OBLIGATORIO);
									map.put("row", i+1);
									map.put("col", 'B');
									break;
							   }
						   }
							
							// CELDA CLAVE
							celClave = rowi.getCell(2);
						
							if(celClave!=null && Cell.CELL_TYPE_STRING == celClave.getCellType()){
								try {
									clave = celClave.toString();		
									/*if(clave.isEmpty()) {
										map.put("rpta", Constantes.EXCEL_RESPUESTA_CERO);
										map.put("message", Constantes.EXCEL_CLAVE_OBLIGATORIA);
										map.put("row", i+1);
										map.put("col", 'C');
										break;
									}else if(clave.trim().length() != 8) {
										map.put("rpta",  Constantes.EXCEL_RESPUESTA_CERO);
										map.put("message",  Constantes.EXCEL_CLAVE_MINIMO_OCHO_CARACTERES);
										map.put("row", i+1);
										map.put("col", 'C');
										break;
									}*/
								} catch (Exception e) {
									clave=null;
								}	
							}else if(celClave!=null && Cell.CELL_TYPE_NUMERIC == celClave.getCellType()){
								int claveAux = Integer.valueOf((int) celClave.getNumericCellValue());
								clave = String.valueOf(claveAux);
								if(clave.trim().length() <8) {
									map.put("rpta",  Constantes.EXCEL_RESPUESTA_CERO);
									map.put("message",  Constantes.EXCEL_CLAVE_MINIMO_OCHO_CARACTERES);
									map.put("row", i+1);
									map.put("col", 'C');
									break;
								}

						   }else{
							   if(clave.isEmpty()) {
									map.put("rpta", Constantes.EXCEL_RESPUESTA_CERO);
									map.put("message", Constantes.EXCEL_CLAVE_OBLIGATORIA);
									map.put("row", i+1);
									map.put("col", 'C');
									break;
							   }
						   }
							
							
							// CELDA CODIGO TRABAJADOR
							celCodTrabajador = rowi.getCell(3);
							
							/*int countTrueRowExcel = 0;
							for(int j=0; j<rowi.getLastCellNum(); j++) {
								cell = (HSSFCell) rowi.getCell(j, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
				
								if(cell.CELL_TYPE_BLANK == cell.getCellType()) {
									contador++;						
								}
								if(contador < 5 && j == 10){
									countTrueRowExcel ++;
								}													
							}
											
							for (int k = 1; k < countTrueRowExcel; k++) {
								Row rowActual = hojaUno.getRow(k);
								
								Row rowSiguiente = hojaUno.getRow(k+1);
								
									if (rowActual != null) {
										String valActual = rowActual.getCell(3).getStringCellValue();
										String valSiguiente = rowSiguiente.getCell(3).getStringCellValue();								
										if(valActual.equals(valSiguiente)) {
											break;
										}
									}
							}*/
						
							if(celCodTrabajador!=null && Cell.CELL_TYPE_STRING == celCodTrabajador.getCellType()){
								try {
									codTrabajador = celCodTrabajador.getStringCellValue();	
									ApiOutResponse apiOutResponse = personaDao.validarDataFormulario(Constantes.VALOR_COD_TRAB, codUsuario, codTrabajador);
									if(apiOutResponse.getCodResultado().equals(1)) {
										map.put("rpta", Constantes.EXCEL_RESPUESTA_CERO);
										map.put("message", Constantes.EXCEL_CODIGO_TRABAJADOR_EXISTE);
										map.put("row", i+1);
										map.put("col", 'D');
										break;
									}
									
								} catch (Exception e) {
									codTrabajador=null;
								}	
							}else if(celCodTrabajador!=null && Cell.CELL_TYPE_NUMERIC == celCodTrabajador.getCellType()){
								int codTrabajadorAux = Integer.valueOf((int) celCodTrabajador.getNumericCellValue());
								codTrabajador = String.valueOf(codTrabajadorAux);

						   }else{
							
							   if(codTrabajador.isEmpty()) {
									map.put("rpta",  Constantes.EXCEL_RESPUESTA_CERO);
									map.put("message", Constantes.EXCEL_CODIGO_TRABAJADOR_OBLIGATORIO);
									map.put("row", i+1);
									map.put("col", 'D');
									break;
								}
						   }
							
							// CELDA PRIMER NOMBRE
							celPriNombre = rowi.getCell(4);
						
							if(celPriNombre!=null && Cell.CELL_TYPE_STRING == celPriNombre.getCellType()){
								try {
									priNombre = celPriNombre.toString();						
								} catch (Exception e) {
									priNombre=null;
								}	
							}else if(celPriNombre!=null && Cell.CELL_TYPE_NUMERIC == celPriNombre.getCellType()){
								int priNombreAux = Integer.valueOf((int) celPriNombre.getNumericCellValue());
								priNombre = String.valueOf(priNombreAux);

						   }else{
							   	if(priNombre.isEmpty()) {
							   		map.put("rpta",  Constantes.EXCEL_RESPUESTA_CERO);
									map.put("message", Constantes.EXCEL_PRIMER_NOMBRE_OBLIGATORIO);
									map.put("row", i+1);
									map.put("col", 'E');
									break;
							   }
						   }	
							
						
							// CELDA SEGUNDO NOMBRE
							celSegNombre = rowi.getCell(5);
						
							if(celSegNombre!=null && Cell.CELL_TYPE_STRING == celSegNombre.getCellType()){
								try {
									segNombre = celSegNombre.toString();						
								} catch (Exception e) {
									segNombre=null;
								}	
							}else if(celSegNombre!=null && Cell.CELL_TYPE_NUMERIC == celSegNombre.getCellType()){
								int segNombreAux = Integer.valueOf((int) celSegNombre.getNumericCellValue());
								segNombre = String.valueOf(segNombreAux);

						   }else{
							   segNombre=null;
						   }	
							
							
							// CELDA APELLIDO PATERNO
							celApePaterno = rowi.getCell(6);
						
							if(celApePaterno!=null && Cell.CELL_TYPE_STRING == celApePaterno.getCellType()){
								try {
									apePaterno = celApePaterno.toString();						
								} catch (Exception e) {
									apePaterno=null;
								}	
							}else if(celApePaterno!=null && Cell.CELL_TYPE_NUMERIC == celApePaterno.getCellType()){
								int apePaternoAux = Integer.valueOf((int) celApePaterno.getNumericCellValue());
								apePaterno = String.valueOf(apePaternoAux);

						   }else{
							   if(apePaterno.isEmpty()) {
									map.put("rpta",  Constantes.EXCEL_RESPUESTA_CERO);
									map.put("message", Constantes.EXCEL_APE_PATERNO_OBLIGATORIO);
									map.put("row", i+1);
									map.put("col", 'G');
									break;
							   }
						   }	
							
							// CELDA APELLIDO MATERNO
							celApeMaterno = rowi.getCell(7);
						
							if(celApeMaterno!=null && Cell.CELL_TYPE_STRING == celApeMaterno.getCellType()){
								try {
									apeMaterno = celApeMaterno.toString();						
								} catch (Exception e) {
									apeMaterno=null;
								}	
							}else if(celApeMaterno!=null && Cell.CELL_TYPE_NUMERIC == celApeMaterno.getCellType()){
								int apeMaternoAux = Integer.valueOf((int) celApeMaterno.getNumericCellValue());
								apeMaterno = String.valueOf(apeMaternoAux);

						   }else{
							   if(apeMaterno.isEmpty()) {
							   		map.put("rpta",  Constantes.EXCEL_RESPUESTA_CERO);
									map.put("message", Constantes.EXCEL_APE_MATERNO_OBLIGATORIO);
									map.put("row", i+1);
									map.put("col", 'H');
									break;
							   }
						   }	
							
							
							// CELDA CORREO ELECTRÓNICO
							celEmail = rowi.getCell(8);
						
							if(celEmail!=null && Cell.CELL_TYPE_STRING == celEmail.getCellType()){
								try {
									email = celEmail.toString();
									boolean formatoValidoCorreo = Util.formatoCorreo(email);
									if(!formatoValidoCorreo) {
										map.put("rpta",  Constantes.EXCEL_RESPUESTA_CERO);
										map.put("message", Constantes.EXCEL_CORREO_FORMATO_INVALIDO);
										map.put("row", i+1);
										map.put("col", 'I');
										break;
									}
								} catch (Exception e) {
									email=null;
								}	
							}else if(celEmail!=null && Cell.CELL_TYPE_NUMERIC == celEmail.getCellType()){
								int emailAux = Integer.valueOf((int) celEmail.getNumericCellValue());
								email = String.valueOf(emailAux);

						   }else{
							   if(email.isEmpty()) {
									map.put("rpta",  Constantes.EXCEL_RESPUESTA_CERO);
									map.put("message", Constantes.EXCEL_CORREO_OBLIGATORIO);
									map.put("row", i+1);
									map.put("col", 'I');
									break;
							   }
						   }	
							
							
							
							// CELDA SEXO
							celSexo = rowi.getCell(9);
						
							if(celSexo!=null && Cell.CELL_TYPE_STRING == celSexo.getCellType()){
								try {
									sexo = celSexo.toString();		
									
									   if(!sexo.trim().toUpperCase().equals("F")  && !sexo.trim().toUpperCase().equals("M") ) {
											map.put("rpta",  Constantes.EXCEL_RESPUESTA_CERO);
											map.put("message", Constantes.EXCEL_SEXO_NO_CORRECTO);
											map.put("row", i+1);
											map.put("col", 'J');
											break;
									   }
									
								} catch (Exception e) {
									sexo=null;
								}	
							}else if(celSexo!=null && Cell.CELL_TYPE_NUMERIC == celSexo.getCellType()){
								int sexoAux = Integer.valueOf((int) celSexo.getNumericCellValue());
								sexo = String.valueOf(sexoAux);

						   }else{
							   sexo = null;
						   }	
							
							
							// CELDA FECHA NACIMIENTO
							celFecNacimiento = rowi.getCell(10);
						
							if(celFecNacimiento!=null && Cell.CELL_TYPE_STRING == celFecNacimiento.getCellType()){
								try {
									fecNacimiento = celFecNacimiento.toString();	
									String fechaNoPermitida = "01-01-1920";
									boolean fechaValida= Util.fechaMenorIgual(fecNacimiento, fechaNoPermitida);
									if(!fechaValida) {
										map.put("rpta",  Constantes.EXCEL_RESPUESTA_CERO);
										map.put("message", Constantes.EXCEL_FECHA_NACIMIENTO_INVALIDA);
										map.put("row", i+1);
										map.put("col", 'K');
										break;
									}
								} catch (Exception e) {
									fecNacimiento=null;
								}	
							}else if(celFecNacimiento!=null && Cell.CELL_TYPE_NUMERIC == celFecNacimiento.getCellType()){
								
								
								
								//fecNacimiento = formatoFecha( celFecNacimiento.toString());
								 SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
								 fecNacimiento = sdf.format(celFecNacimiento.getDateCellValue());
								String fechaNoPermitida = "1920/01/01";
								boolean fechaValida= Util.fechaMenorIgual(fecNacimiento, fechaNoPermitida);
								if(!fechaValida) {
									map.put("rpta",  Constantes.EXCEL_RESPUESTA_CERO);
									map.put("message", Constantes.EXCEL_FECHA_NACIMIENTO_INVALIDA);
									map.put("row", i+1);
									map.put("col", 'K');
									break;
								}

						   }else{
							   
							   if(fecNacimiento.isEmpty()) {
								   	map.put("rpta",  Constantes.EXCEL_RESPUESTA_CERO);
									map.put("message", Constantes.EXCEL_FECHA_NACIMIENTO_OBLIGATORIO);
									map.put("row", i+1);
									map.put("col", 'K');
									break;
							   }
						   }	
							
							
							// CELDA FECHA INGRESO
							celFecIngreso = rowi.getCell(11);
						
							if(celFecIngreso!=null && Cell.CELL_TYPE_STRING == celFecIngreso.getCellType()){
								try {
									fecIngreso = celFecIngreso.toString();				
									
									boolean fechaValida= Util.fechaMenorIgual(fecNacimiento, fecIngreso);
									if(fechaValida) {
									  	map.put("rpta",  Constantes.EXCEL_RESPUESTA_CERO);
										map.put("message", Constantes.EXCEL_FECHA_INGRESO_MAYOR_FECHA_NACIMIENTO);				
										map.put("row", i+1);
										map.put("col", 'L');
										break;
									}
									
									
								} catch (Exception e) {
									fecIngreso=null;
								}	
							}else if(celFecIngreso!=null && Cell.CELL_TYPE_NUMERIC == celFecIngreso.getCellType()){
							
								//fecIngreso =  formatoFecha(celFecIngreso.toString());
								
								 SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
								 fecIngreso = sdf.format(celFecIngreso.getDateCellValue());
								
										
								
								boolean fechaValida= Util.fechaMenorIgual(fecNacimiento, fecIngreso);
								if(fechaValida) {
								  	map.put("rpta",  Constantes.EXCEL_RESPUESTA_CERO);
									map.put("message", Constantes.EXCEL_FECHA_INGRESO_MAYOR_FECHA_NACIMIENTO);				
									map.put("row", i+1);
									map.put("col", 'L');
									break;
								}

						   }else{
							   if(fecIngreso.isEmpty()) {
								   	map.put("rpta",  Constantes.EXCEL_RESPUESTA_CERO);
									map.put("message", Constantes.EXCEL_FECHA_INGRESO_OBLIGATORIO);				
									map.put("row", i+1);
									map.put("col", 'L');
									break;
							   }
						   }	
							
							
							// CELDA SUELDO BASICO
							celSueBasico = rowi.getCell(12);
							
							if(celSueBasico!=null && Cell.CELL_TYPE_STRING == celSueBasico.getCellType()){
								try {
									sueBasico = BigDecimal.valueOf(celSueBasico.getNumericCellValue());					
								} catch (Exception e) {
									sueBasico=BigDecimal.ZERO;
								}	
							}else if(celSueBasico!=null && Cell.CELL_TYPE_NUMERIC == celSueBasico.getCellType()){
								sueBasico=BigDecimal.valueOf(celSueBasico.getNumericCellValue());
								
								if(sueBasico.compareTo(BigDecimal.ZERO) < 0) {
								 	map.put("rpta",  Constantes.EXCEL_RESPUESTA_CERO);
									map.put("message", Constantes.EXCEL_SUELDO_NEGATIVO);				
									map.put("row", i+1);
									map.put("col", 'M');
									break;
								}
								

						   }else{
							   sueBasico=BigDecimal.ZERO;
						   }	
							
											
							// CELDA TIPO DOCUMENTO
							celTipDocumento = rowi.getCell(13);
							
							if(celTipDocumento!=null && Cell.CELL_TYPE_STRING == celTipDocumento.getCellType()){
								try {
									tipDocumento = celTipDocumento.toString();	
									
									ApiOutResponse apiOutResponse = buscarDato(Constantes.TIPO_DOCUMENTO_EXCEL, tipDocumento);
									if(apiOutResponse.getCodResultado() == 0) {			
										map.put("rpta",  Constantes.EXCEL_RESPUESTA_CERO);
										map.put("message", Constantes.EXCEL_TIPO_DOCUMENTO_NO_EXISTE);				
										map.put("row", i+1);
										map.put("col", 'N');
										break;
									}else {
										codTipDoc = Integer.parseInt(apiOutResponse.getResponse().toString());
									
									}
								} catch (Exception e) {
									tipDocumento=null;
								}	
							}else if(celTipDocumento!=null && Cell.CELL_TYPE_NUMERIC == celTipDocumento.getCellType()){
								int tipDocumentoAux = Integer.valueOf((int) celTipDocumento.getNumericCellValue());
								tipDocumento = String.valueOf(tipDocumentoAux);

						   }else{
							   codTipDoc = null;
						   }	
							
							// CELDA NRO. DOCUMENTO
							celNumDocumento = rowi.getCell(14);
							
							if(celNumDocumento!=null && Cell.CELL_TYPE_STRING == celNumDocumento.getCellType()){
								try {
									numDocumento = celNumDocumento.toString();						
								} catch (Exception e) {
									numDocumento=null;
								}	
							}else if(celNumDocumento!=null && Cell.CELL_TYPE_NUMERIC == celNumDocumento.getCellType()){
								int numDocumentoAux = Integer.valueOf((int) celNumDocumento.getNumericCellValue());
								numDocumento = String.valueOf(numDocumentoAux);

						   }else{
							   if(numDocumento.isEmpty()) {
								   	map.put("rpta",  Constantes.EXCEL_RESPUESTA_CERO);
									map.put("message", Constantes.EXCEL_NUMERO_DOCUMENTO_OBLIGATORIO);				
									map.put("row", i+1);
									map.put("col", 'O');
									break;
							   }
						   }	
							
							// CELDA CARGO
							celCargo = rowi.getCell(15);
							
							if(celCargo!=null && Cell.CELL_TYPE_STRING == celCargo.getCellType()){
								try {
									cargo = celCargo.toString();		
									
									ApiOutResponse apiOutResponse = buscarDato(Constantes.CARGO, cargo);
									if(apiOutResponse.getCodResultado() == 0) {
										
										map.put("rpta",  Constantes.EXCEL_RESPUESTA_CERO);
										map.put("message", Constantes.EXCEL_CARGO_NO_EXISTE);				
										map.put("row", i+1);
										map.put("col", 'P');
										break;
									}else {
										codCargo = Integer.parseInt(apiOutResponse.getResponse().toString());
									
									}
								} catch (Exception e) {
									cargo=null;
								}	
							}else if(celCargo!=null && Cell.CELL_TYPE_NUMERIC == celCargo.getCellType()){
								int cargoAux = Integer.valueOf((int) celCargo.getNumericCellValue());
								cargo = String.valueOf(cargoAux);

						   }else{
							   codCargo=null;
						   }	
							
							
							// CELDA CATEGORIA
							celCategoria = rowi.getCell(16);
							
							if(celCategoria!=null && Cell.CELL_TYPE_STRING == celCategoria.getCellType()){
								try {
									categoria = celCategoria.toString();
									
									ApiOutResponse apiOutResponse = buscarDato(Constantes.CATEGORIA, categoria);
									if(apiOutResponse.getCodResultado() == 0) {
										map.put("rpta",  Constantes.EXCEL_RESPUESTA_CERO);
										map.put("message", Constantes.EXCEL_CATEGORIA_NO_EXISTE);				
										map.put("row", i+1);
										map.put("col", 'Q');
										break;
									}else {
										codCategoria = Integer.parseInt(apiOutResponse.getResponse().toString());
									
									}
								} catch (Exception e) {
									categoria=null;
								}	
							}else if(celCategoria!=null && Cell.CELL_TYPE_NUMERIC == celCategoria.getCellType()){
								int categoriaAux = Integer.valueOf((int) celCategoria.getNumericCellValue());
								categoria = String.valueOf(categoriaAux);

						   }else{
							   codCategoria=null;
						   }	
							
							// CELDA AREA
							celArea = rowi.getCell(17);
							
							if(celArea!=null && Cell.CELL_TYPE_STRING == celArea.getCellType()){
								try {
									area = celArea.toString();		
									ApiOutResponse apiOutResponse = buscarDato(Constantes.AREA, area);
									if(apiOutResponse.getCodResultado() == 0) {							
										map.put("rpta",  Constantes.EXCEL_RESPUESTA_CERO);
										map.put("message", Constantes.EXCEL_AREA_NO_EXISTE);				
										map.put("row", i+1);
										map.put("col", 'R');
										break;
									}else {
										codArea = Integer.parseInt(apiOutResponse.getResponse().toString());
									
									}
								} catch (Exception e) {
									area=null;
								}	
							}else if(celArea!=null && Cell.CELL_TYPE_NUMERIC == celArea.getCellType()){
								int areaAux = Integer.valueOf((int) celArea.getNumericCellValue());
								area = String.valueOf(areaAux);

						   }else{
							   codArea=null;
						   }	
							
							// CELDA LOCAL
							celLocal = rowi.getCell(18);
							
							if(celLocal!=null && Cell.CELL_TYPE_STRING == celLocal.getCellType()){
								try {
									local = celLocal.toString();		
									ApiOutResponse apiOutResponse = buscarDato(Constantes.LOCAL, local);
									if(apiOutResponse.getCodResultado() == 0) {		
										map.put("rpta",  Constantes.EXCEL_RESPUESTA_CERO);
										map.put("message", Constantes.EXCEL_LOCAL_NO_EXISTE);				
										map.put("row", i+1);
										map.put("col", 'S');
										break;
									}else {
										codLocal = Integer.parseInt(apiOutResponse.getResponse().toString());
									
									}
								} catch (Exception e) {
									local=null;
								}	
							}else if(celLocal!=null && Cell.CELL_TYPE_NUMERIC == celLocal.getCellType()){
								int localAux = Integer.valueOf((int) celLocal.getNumericCellValue());
								local = String.valueOf(localAux);

						   }else{
							   codLocal=null;
						   }	
							
							
							// CELDA ROL
							celRol = rowi.getCell(19);
							
							if(celRol!=null && Cell.CELL_TYPE_STRING == celRol.getCellType()){
								try {
									rol = celRol.toString();	
									ApiOutResponse apiOutResponse = buscarDato(Constantes.ROL, rol);
									if(apiOutResponse.getCodResultado() == 0) {
										map.put("rpta",  Constantes.EXCEL_RESPUESTA_CERO);
										map.put("message", Constantes.EXCEL_ROL_NO_EXISTE);				
										map.put("row", i+1);
										map.put("col", 'T');
										break;
									}else {
										codRol = Integer.parseInt(apiOutResponse.getResponse().toString());
									
									}
								} catch (Exception e) {
									rol=null;
								}	
							}else if(celRol!=null && Cell.CELL_TYPE_NUMERIC == celRol.getCellType()){
								int rolAux = Integer.valueOf((int) celRol.getNumericCellValue());
								rol = String.valueOf(rolAux);

						   }else{
							   codRol=null;
						   }	
							
							
							// CELDA sO
							celEstado = rowi.getCell(20);
							
							if(celEstado!=null && Cell.CELL_TYPE_STRING == celEstado.getCellType()){
								try {
									estado = celEstado.toString();		
									
									ApiOutResponse apiOutResponse = buscarDato(Constantes.ESTADO, estado);
									if(apiOutResponse.getCodResultado() == 0) {
										map.put("rpta",  Constantes.EXCEL_RESPUESTA_CERO);
										map.put("message", Constantes.EXCEL_ESTADO_NO_EXISTE);				
										map.put("row", i+1);
										map.put("col", 'U');
										break;
									}else {
										codEstado = Integer.parseInt(apiOutResponse.getResponse().toString());
									
									}
								} catch (Exception e) {
									estado=null;
								}	
							}else if(celEstado!=null && Cell.CELL_TYPE_NUMERIC == celEstado.getCellType()){
								int estadoAux = Integer.valueOf((int) celEstado.getNumericCellValue());
								estado = String.valueOf(estadoAux);

						   }else{
							   estado=null;
						   }	
							
							objPersona =  new Persona();
							objPersona.setCodUsuario(codUsuario);
							objPersona.setUsuario(usuario);
							objPersona.setClave(clave);
							objPersona.setCodTrabajador(codTrabajador);
							objPersona.setPrimerNombre(priNombre);
							objPersona.setSegundoNombre(segNombre);
							objPersona.setApePaterno(apePaterno);
							objPersona.setApeMaterno(apeMaterno);
							objPersona.setCorreoElectronico(email);
							objPersona.setSexo(sexo);
							objPersona.setFecNacimiento(formatoFecha(fecNacimiento));
							objPersona.setFecIngreso(formatoFecha(fecIngreso));
							objPersona.setSueldoBasico(sueBasico);
							objPersona.setIdTipoDocumento(codTipDoc);
							objPersona.setNumDocumento(numDocumento);
							objPersona.setIdCargo(codCargo);
							objPersona.setIdCategoria(codCategoria);
							objPersona.setIdArea(codArea);
							objPersona.setIdLocal(codLocal);
							objPersona.setIdRol(codRol);
							objPersona.setIdEstado(codEstado);
							objPersona.setFlagEditar(flagAccion);
							
							lstPersona.add(objPersona);
							
							
							PersonaExcelAux objPersonaExcelAux =  new PersonaExcelAux();
							objPersonaExcelAux.setCodTrabajador(codTrabajador);
							objPersonaExcelAux.setNumDocumento(numDocumento);
							objPersonaExcelAux.setFilaExcel(i+1);
							
							lstPersonaExcelAux.add(objPersonaExcelAux);
							
							
							 empresa ="";
							 usuario ="";
							 clave ="";
							 codTrabajador ="";
							 priNombre ="";
							 segNombre ="";
							 apePaterno ="";
							 apeMaterno ="";
							 email ="";
							 sexo ="";
							 fecNacimiento ="";
							 fecIngreso ="";
							 sueBasico =  BigDecimal.ZERO;
							 tipDocumento ="";
							 numDocumento ="";
							 cargo ="";
							 categoria ="";
							 area ="";
							 local ="";
							 rol ="";
							 estado ="";
							 flagAccion = 0;
						
						}else {
							break;
						}
					 
					}
				
				}
				
				
			
				 HashMap<String, Object> repetido =  fileDao.encontrarRepetidos(lstPersonaExcelAux);
				
				 if(repetido.get("trabajadorRepetido")!=null) {
			    	  PersonaExcelAux objPersonaExcelAux =    (PersonaExcelAux) repetido.get("trabajadorRepetido");
			    	  objPersonaExcelAux.setColumnaExcel("D");
			    	  
			    		map.put("rpta",  Constantes.EXCEL_RESPUESTA_CERO);
						map.put("message", Constantes.EXCEL_CODIGO_TRABAJADOR_REPETIDO);				
						map.put("row", objPersonaExcelAux.getFilaExcel());
						map.put("col", objPersonaExcelAux.getColumnaExcel());
			    	
			    			  
			    }else if(repetido.get("documentoRepetido")!=null){
			    	  PersonaExcelAux objPersonaExcelAux =    (PersonaExcelAux) repetido.get("documentoRepetido");
			    	  objPersonaExcelAux.setColumnaExcel("O");
			    	  map.put("rpta",  Constantes.EXCEL_RESPUESTA_CERO);
					  map.put("message", Constantes.EXCEL_NUMERO_DOCUMENTO_REPETIDO);				
					  map.put("row", objPersonaExcelAux.getFilaExcel());
					  map.put("col", objPersonaExcelAux.getColumnaExcel());
			    }
			  
			    
				 
				 
				if(map.isEmpty()) {
		       
			        for (MensajeError error : listaErrores) {
						System.out.println(error.getDescripcion());
					}
			       if(listaErrores.isEmpty()){
			    	   ApiOutResponse out= new ApiOutResponse();
			    	   rptaInsert = personaDao.grabarPersonaMasivo(lstPersona);
			    	   if(rptaInsert == 1) {
			    		   map.put("rpta", "ok");
			    	   }
			    	 
						
			    	   System.out.println("inserto : " + rptaInsert);
			    	  // out=subirArchivo(  dato,  fileInput, fileMultipart,   pdescripcion,  "007");
			    	  /* if(out.getCodResultado()==1){
			    		   
			    		   rpta = ejecucionDao.cargarRegistrosLista(presupuesto, presupuestoDetalle,  dato);
			    		   System.out.println("whr>>"+rpta);
			    	   }else {
			    		   rpta = "";
			    	   }*/
			    	} else{
			    	   sessionExcel.setAttribute("listaErroresPresupuesto", listaErrores);
			    	   HttpSession misession= (HttpSession) request.getSession();
			    	   misession.getAttribute("listaErroresPresupuesto");
			      	   rpta = "0";//tiene errores por lo tanto mostrar la lista de errores
			      	}
				}
			}
			fileInputStream.close();
		} catch (FileNotFoundException e) {
			log.error(MSJ_ERROR_NO_ENCUENTRA_ARCHIVO + e);
			rpta = "";
		} catch (IOException e) {
			log.error(MSJ_ERROR_IO_ARCHIVO + e);
			rpta = "";
		} catch (EncryptedDocumentException e1) {
			
			e1.printStackTrace();
		} catch (InvalidFormatException e1) {

			e1.printStackTrace();
		}finally {
			
				try {
					libro.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
		}
		return map;
	}
	
	
	private String formatoFecha(String fecha) {
		String date = null;
		try {
		Date objDate = new Date(fecha);
		String pattern = "yyyy/MM/dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		date = simpleDateFormat.format(objDate);
		System.out.println(date);
		}catch(Exception e) {
			System.out.println(e);
		}
		
		
		return date;
	}
	
	private String agregarErrorBeanConMensaje(String mensaje, List<MensajeError> listaErrores){		
		MensajeError error = new MensajeError();
		error.setDescripcion(mensaje);
		listaErrores.add(error);
		return mensaje;
	}

	@Override
	public byte[] verTxtErrores(String nombreListado, HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ApiOutResponse buscarDato(String dato, String valor) {
		// TODO Auto-generated method stub
		return fileDao.buscarDato(dato, valor);
	}

}

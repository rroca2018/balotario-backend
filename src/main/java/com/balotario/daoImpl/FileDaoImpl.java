package com.balotario.daoImpl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import org.springframework.web.multipart.MultipartFile;

import com.balotario.api.ApiOutResponse;
import com.balotario.bean.PersonaExcelAux;
import com.balotario.dao.FileDao;
import com.balotario.dto.PersonaDTO;
import com.balotario.entity.Persona;
import com.balotario.util.Constantes;


@Repository
public class FileDaoImpl implements FileDao{
	
	
	private  final Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcCall jdbcCall;
	

	@Override
	public ApiOutResponse validarCargaExcelPersona(Long idUsuario, MultipartFile file, HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public byte[] verTxtErrores(String nombreListado, HttpServletRequest request) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ApiOutResponse buscarDato(String dato, String valor) {
		log.info("[DAO obtenerPersona] INICIO  ");	
		ApiOutResponse outResponse = new ApiOutResponse();
		int codigoRpta=0;
		String mensajeRpta="";
		try {
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate.getDataSource());
			jdbcCall.withSchemaName("dbo")
			.withProcedureName("buscarDato")
			.declareParameters(
					new SqlParameter("DATO", Types.VARCHAR),
					new SqlParameter("VALOR", Types.VARCHAR),
					new SqlOutParameter("COD_RESULTADO", 	Types.VARCHAR),
					new SqlOutParameter("MSG_RESULTADO", 	Types.VARCHAR)
				);
	
			MapSqlParameterSource parametros = new MapSqlParameterSource();
			parametros.addValue("DATO",  dato);
			parametros.addValue("VALOR",  valor);

			
			Map<String, Object> results = jdbcCall.execute(parametros);
			List<Map<String, Object>> rs = (List<Map<String, Object>>) results.get(Constantes.RETURN_RESULT_SET_PREFIX);
			
			codigoRpta=Integer.parseInt(results.get("COD_RESULTADO").toString());
			mensajeRpta=results.get("MSG_RESULTADO").toString();
			
			if (results.isEmpty()||results.size()<1) {
				log.info("Sin registros, verificar el procedimiento obtenerPersona>>");
				return  new ApiOutResponse();
			}
			
			if (codigoRpta!=1) {//VERIFICACION DE ERROR EN LA BD
				log.error("[listarItems] Ocurrio un error en la operacion del Procedimiento almacenado listadoPersona : "+ mensajeRpta);
				outResponse.setCodResultado(codigoRpta);
				outResponse.setTotal(0);
				outResponse.setResponse("Error en la BD");
				outResponse.setMsgResultado(mensajeRpta);
				return outResponse;
			}
			
			
			if(rs!=null) {
				Object obj = new Object();
					if(dato.equals(Constantes.USUARIO)) {
						Map<String, Object> map = rs.get(0);
						obj = Integer.parseInt(map.get("Co_Usuario").toString());
							
						outResponse.setResponse(obj);
					}else if(dato.equals(Constantes.TIPO_DOCUMENTO_EXCEL)) {
							Map<String, Object> map = rs.get(0);
							obj = Integer.parseInt(map.get("Co_TipoDocumento").toString());
								
							outResponse.setResponse(obj);
					}else if(dato.equals(Constantes.CARGO)) {
						Map<String, Object> map = rs.get(0);
						obj = Integer.parseInt(map.get("Co_Cargo").toString());
							
						outResponse.setResponse(obj);
					}else if(dato.equals(Constantes.CATEGORIA)) {
						Map<String, Object> map = rs.get(0);
						obj = Integer.parseInt(map.get("Co_Categoria").toString());
							
						outResponse.setResponse(obj);
					}else if(dato.equals(Constantes.AREA)) {
						Map<String, Object> map = rs.get(0);
						obj = Integer.parseInt(map.get("Co_Area").toString());
							
						outResponse.setResponse(obj);
					}else if(dato.equals(Constantes.LOCAL)) {
						Map<String, Object> map = rs.get(0);
						obj = Integer.parseInt(map.get("Co_Local").toString());
							
						outResponse.setResponse(obj);
					}else if(dato.equals(Constantes.ROL)) {
						Map<String, Object> map = rs.get(0);
						obj = Integer.parseInt(map.get("Co_Rol").toString());
							
						outResponse.setResponse(obj);
					}else if(dato.equals(Constantes.ESTADO)) {
						Map<String, Object> map = rs.get(0);
						obj = Integer.parseInt(map.get("Co_Estado").toString());
							
						outResponse.setResponse(obj);
					}
					outResponse.setCodResultado( results.get("COD_RESULTADO")!=null ? Integer.parseInt(results.get("COD_RESULTADO").toString()) : 500);
					outResponse.setMsgResultado( results.get("MSG_RESULTADO")!=null ? results.get("MSG_RESULTADO").toString() : "-" );
				
			}
			
		} catch (Exception e) {
			log.error("DAO listadoPersona>>>>"+this.getClass().getName(), e);
			outResponse.setCodResultado(500);
			outResponse.setMsgResultado( "Error en la Base de datos!.");
		}
		log.info("[DAO listadoPersona] FIN  ");	
		return outResponse;
	}

	@Override
	public HashMap<String, Object> encontrarRepetidos(List<PersonaExcelAux> lstPersona) {
		HashMap<String, Object> resultado = new HashMap<String, Object>();
		outerloop:
		for (int i=0;i<lstPersona.size();i++)
		{
			for(int j=0;j<lstPersona.size();j++) {
				if(i != j) {
					if ( lstPersona.get(i).getCodTrabajador().equals(lstPersona.get(j).getCodTrabajador())){	
						resultado.put("trabajadorRepetido",  lstPersona.get(i));
						System.out.println("repetido trabajador: " +  lstPersona.get(i).getCodTrabajador());
						break outerloop;
						
					}
					if(lstPersona.get(i).getNumDocumento().equals(lstPersona.get(j).getNumDocumento())) {
						resultado.put("documentoRepetido", lstPersona.get(i));
						System.out.println("repetido documento: " +  lstPersona.get(i).getNumDocumento());
						break outerloop;
					}
				}			
			}
	
		}
		
		return resultado;
		
	}
	
}

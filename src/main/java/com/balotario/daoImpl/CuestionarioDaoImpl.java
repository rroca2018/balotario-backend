package com.balotario.daoImpl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

import com.balotario.api.ApiOutResponse;
import com.balotario.dao.CuestionarioDao;
import com.balotario.entity.Pregunta;
import com.balotario.entity.Respuesta;
import com.balotario.util.Constantes;

@Repository
public class CuestionarioDaoImpl implements CuestionarioDao {

	private final Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcCall jdbcCall;
	
	@Override
	public ApiOutResponse listaPregunta(Long cantidadPregunta,Long idBalotario) {
		
		jdbcCall = new SimpleJdbcCall(jdbcTemplate.getDataSource());
		ApiOutResponse outResponse = new ApiOutResponse();
		int codigoRpta = 0;
		String mensajeRpta = "";
		
		List<Pregunta> listaPregunta = new ArrayList<>();
		List<Respuesta> listaRespuesta = null;
		
		try {
			
			jdbcCall
			.withSchemaName("dbo")
			.withProcedureName("listadoPreguntaRespuesta")
			.declareParameters(
					new SqlParameter("OPCION", Types.VARCHAR), 
					new SqlParameter("ID_PREGUNTA", Types.BIGINT),
					new SqlParameter("CANTIDAD_PREGUNTA", Types.BIGINT),
					new SqlParameter("ID_BALOTARIO", Types.BIGINT),
					// RETORNO
					new SqlOutParameter("COD_RESULTADO", Types.VARCHAR),
					new SqlOutParameter("MSG_RESULTADO", Types.VARCHAR), 
					new SqlOutParameter("TOTAL", Types.BIGINT));
			MapSqlParameterSource parametros = new MapSqlParameterSource();
			parametros.addValue("OPCION", "LISTADO_PREGUNTA");
			parametros.addValue("ID_PREGUNTA",0);
			parametros.addValue("CANTIDAD_PREGUNTA",cantidadPregunta);
			parametros.addValue("ID_BALOTARIO",idBalotario);
			
			Map<String, Object> results = jdbcCall.execute(parametros);
			List<Map<String, Object>> rs = (List<Map<String, Object>>) results.get(Constantes.RETURN_RESULT_SET_PREFIX);
			
			
			codigoRpta = Integer.parseInt(results.get("COD_RESULTADO").toString());
			mensajeRpta = results.get("MSG_RESULTADO").toString();
			
			if (codigoRpta != 1) {
				outResponse.setCodResultado(Integer.parseInt(results.get("COD_RESULTADO").toString()));
				outResponse.setTotal(0);
				outResponse.setResponse("Error en la BD");
				outResponse.setMsgResultado(results.get("MSG_RESULTADO").toString());
				return outResponse;
			}
			
			if (rs.size() > 0) {
				for (Map<String, Object> map : rs) {
					Pregunta item = new Pregunta();
					item.setIdPregunta(Long.valueOf(map.get("Co_Pregunta").toString()));
					item.setNombrePregunta(map.get("No_DescPregunta").toString());
					item.setIdRespuestaCorrecto(Long.valueOf(map.get("Co_Respuesta").toString()));
					
					// ******************************* DETALLE  *******************************
					jdbcCall
					.withSchemaName("dbo")
					.withProcedureName("listadoPreguntaRespuesta")
					.declareParameters(
							new SqlParameter("OPCION", Types.VARCHAR), 
							new SqlParameter("ID_PREGUNTA", Types.BIGINT),
							new SqlParameter("CANTIDAD_PREGUNTA", Types.BIGINT),
							new SqlParameter("ID_BALOTARIO", Types.BIGINT),
							// RETORNO
							new SqlOutParameter("COD_RESULTADO", Types.VARCHAR),
							new SqlOutParameter("MSG_RESULTADO", Types.VARCHAR), 
							new SqlOutParameter("TOTAL", Types.BIGINT));
					MapSqlParameterSource parametros2 = new MapSqlParameterSource();
					parametros2.addValue("OPCION", "LISTADO_RESPUESTA");
					parametros2.addValue("ID_PREGUNTA",Long.valueOf(map.get("Co_Pregunta").toString()));
					parametros2.addValue("CANTIDAD_PREGUNTA",0);
					parametros2.addValue("ID_BALOTARIO",0);
					
					Map<String, Object> results2 = jdbcCall.execute(parametros2);
					List<Map<String, Object>> rs2 = (List<Map<String, Object>>) results2.get(Constantes.RETURN_RESULT_SET_PREFIX);
					
					listaRespuesta = new ArrayList<>();
					
					if (rs2.size() > 0) {
						for (Map<String, Object> map2 : rs2) {
							Respuesta item2 = new Respuesta();
							item2.setIdRespuesta(Long.valueOf(map2.get("Co_Respuesta").toString()));
							item2.setNombreRespuesta(map2.get("No_DescRespuesta").toString());
							listaRespuesta.add(item2);
						}
						item.setRespuesta(listaRespuesta);
					}
					
					listaPregunta.add(item);
				}
				outResponse.setResponse(listaPregunta);
				outResponse.setTotal(Integer.parseInt(results.get("TOTAL").toString()));
				outResponse.setCodResultado(results.get("COD_RESULTADO") != null ? Integer.parseInt(results.get("COD_RESULTADO").toString()): 500);
				outResponse.setMsgResultado(results.get("MSG_RESULTADO") != null ? results.get("MSG_RESULTADO").toString() : "-");
			} else {
				outResponse.setCodResultado(2);
				outResponse.setTotal(0);
				outResponse.setResponse("");
				outResponse.setMsgResultado("¡No existen registros!");
			}
						
		} catch (Exception e) {
			log.error(">> " + this.getClass().getName(), e);
			outResponse.setCodResultado(500);
			outResponse.setMsgResultado("Error en la Base de datos!.");
		}
		
		return outResponse;
	}

}

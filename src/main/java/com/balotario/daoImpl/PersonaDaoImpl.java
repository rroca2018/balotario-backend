package com.balotario.daoImpl;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.balotario.bean.Item;
import com.balotario.api.ApiOutResponse;
import com.balotario.dao.PersonaDao;
import com.balotario.dto.PersonaDTO;
import com.balotario.entity.Persona;
import com.balotario.util.Constantes;



@Repository
public class PersonaDaoImpl implements PersonaDao {

	
	private  final Log log = LogFactory.getLog(getClass());
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcCall jdbcCall;
	
	@Override
	public ApiOutResponse listarItems(String tipo, int idCategoria, int idArea) {
		log.info("[DAO listarItems] INICIO  ");	
		ApiOutResponse outResponse = new ApiOutResponse();
		List<Item> estados = new ArrayList<>();
		int codigoRpta=0;
		String mensajeRpta="";
		try {
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate.getDataSource());
			jdbcCall.withSchemaName("dbo")
			.withProcedureName("listadoCombo")
			.declareParameters(
					new SqlParameter("TIPO", 		Types.VARCHAR),
					new SqlParameter("CATEGORIA", 	Types.INTEGER),
					new SqlParameter("AREA", 		Types.INTEGER),
					new SqlOutParameter("COD_RESULTADO", 	Types.VARCHAR),
					new SqlOutParameter("MSG_RESULTADO", 	Types.VARCHAR)
				);
	
			MapSqlParameterSource parametros = new MapSqlParameterSource();
			parametros.addValue("TIPO", tipo);
			parametros.addValue("CATEGORIA", idCategoria);
			parametros.addValue("AREA", idArea);
			
			Map<String, Object> results = jdbcCall.execute(parametros);
			List<Map<String, Object>> rs = (List<Map<String, Object>>) results.get(Constantes.RETURN_RESULT_SET_PREFIX);
			
			codigoRpta=Integer.parseInt(results.get("COD_RESULTADO").toString());
			mensajeRpta=results.get("MSG_RESULTADO").toString();
			
			if (results.isEmpty()||results.size()<1) {
				log.info("Sin registros, verificar el procedimiento filtrosProyecto>> tipo:"+tipo);
				return  new ApiOutResponse();
			}
			
			if (codigoRpta!=1) {//VERIFICACION DE ERROR EN LA BD
				log.error("[listarItems] Ocurrio un error en la operacion del Procedimiento almacenado listarItems : "+mensajeRpta);
				outResponse.setCodResultado(codigoRpta);
				outResponse.setTotal(0);
				outResponse.setResponse("Error en la BD");
				outResponse.setMsgResultado(mensajeRpta);
				return outResponse;
			}
			
			if(rs!=null) {
				if (rs.size() > 0) {
					for (Map<String, Object> map : rs) {
						Item estado = new Item();
						estado.setIdCodigo(Integer.parseInt(map.get("ID_CODIGO").toString()) );
						estado.setCidNombre(map.get("CID_NOMBRE")!=null ? map.get("CID_NOMBRE").toString() : "" );
						estado.setIdCategoria(map.get("ID_CATEGORIA")!=null ? Integer.parseInt(map.get("ID_CATEGORIA").toString()) : 0);
						
						estados.add(estado);
					}
					outResponse.setResponse(estados);
					outResponse.setCodResultado( results.get("COD_RESULTADO")!=null ? Integer.parseInt(results.get("COD_RESULTADO").toString()) : 500);
					outResponse.setMsgResultado( results.get("MSG_RESULTADO")!=null ? results.get("MSG_RESULTADO").toString() : "-" );
				}else {
					outResponse.setCodResultado(2);//Codigo para indicar que no existen registros en la BD
					outResponse.setTotal(0);
					outResponse.setResponse("");
					outResponse.setMsgResultado("¡No existen registros!");
				}
			}
			
		} catch (Exception e) {
			log.error("DAO listarItems>>>>"+tipo+">>"+this.getClass().getName(), e);
			outResponse.setCodResultado(500);
			outResponse.setMsgResultado( "Error en la Base de datos!.");
		}
		log.info("[DAO listarItems] FIN  ");	
		return outResponse;
	}

	@Transactional
	@Override
	public int grabarPersona(Persona persona) {
		log.info("[DAO grabarPersona] INICIO  ");	
		
		int codigoRpta=0;
		String mensajeRpta="";
		try {
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate.getDataSource());
			jdbcCall.withSchemaName("dbo")
			.withProcedureName("grabarUsuario")
			.declareParameters(
					new SqlParameter("CoUsuario", 		Types.INTEGER),
					new SqlParameter("NoUsuario", 		Types.VARCHAR),
					new SqlParameter("NoClave", 		Types.VARCHAR),
					new SqlParameter("CoTrabajador", 	Types.VARCHAR),
					new SqlParameter("NoPrimerNombre", 	Types.VARCHAR),
					new SqlParameter("NoSegundoNombre", Types.VARCHAR),
					new SqlParameter("NoApePaterno", 	Types.VARCHAR),
					new SqlParameter("NoApeMaterno", 	Types.VARCHAR),
					new SqlParameter("NoCorreo", 		Types.VARCHAR),
					new SqlParameter("FiSexo", 			Types.VARCHAR),
					new SqlParameter("FeNacimiento", 	Types.VARCHAR),
					new SqlParameter("FeIngreso", 		Types.VARCHAR),
					new SqlParameter("DeSueldo", 		Types.DECIMAL),
					new SqlParameter("CoTipoDocumento", Types.INTEGER),
					new SqlParameter("CoNumDocumento", 	Types.VARCHAR),
					new SqlParameter("CoCargo", 		Types.INTEGER),
					new SqlParameter("CoCategoria", 	Types.INTEGER),
					new SqlParameter("CoArea", 			Types.INTEGER),
					new SqlParameter("CoLocal", 		Types.INTEGER),
					new SqlParameter("CoRol", 			Types.INTEGER),
					new SqlParameter("CoEst", 			Types.INTEGER),
					new SqlParameter("flagEditar", 		Types.INTEGER),
					new SqlOutParameter("COD_RESULTADO",	Types.VARCHAR),
					new SqlOutParameter("MSG_RESULTADO", 	Types.VARCHAR)
				);
			
			
		
	
			MapSqlParameterSource parametros = new MapSqlParameterSource();
			parametros.addValue("CoUsuario", persona.getCodUsuario());
			parametros.addValue("NoUsuario", persona.getUsuario());
			parametros.addValue("NoClave", persona.getClave());
			parametros.addValue("CoTrabajador", persona.getCodTrabajador());
			parametros.addValue("NoPrimerNombre", persona.getPrimerNombre());
			parametros.addValue("NoSegundoNombre", persona.getSegundoNombre());
			parametros.addValue("NoApePaterno", persona.getApePaterno());
			parametros.addValue("NoApeMaterno", persona.getApeMaterno());
			parametros.addValue("NoCorreo", persona.getCorreoElectronico());
			parametros.addValue("FiSexo", persona.getSexo());
			parametros.addValue("FeNacimiento", persona.getFecNacimiento());
			parametros.addValue("FeIngreso", persona.getFecIngreso());
			parametros.addValue("DeSueldo", persona.getSueldoBasico());
			parametros.addValue("CoTipoDocumento", (persona.getIdTipoDocumento() == null)?null:persona.getIdTipoDocumento());
			parametros.addValue("CoNumDocumento", persona.getNumDocumento());
			parametros.addValue("CoCargo", persona.getIdCargo());
			parametros.addValue("CoCategoria", persona.getIdCategoria());
			parametros.addValue("CoArea", persona.getIdArea());
			parametros.addValue("CoLocal", persona.getIdLocal());
			parametros.addValue("CoRol", persona.getIdRol());
			parametros.addValue("CoEst", persona.getIdEstado());
			parametros.addValue("flagEditar", persona.getFlagEditar());
			
			Map<String, Object> results = jdbcCall.execute(parametros);
			
			codigoRpta=Integer.parseInt(results.get("COD_RESULTADO").toString());
			mensajeRpta=results.get("MSG_RESULTADO").toString();
			
			if (results.isEmpty()||results.size()<1) {
				log.info("Sin registros, verificar el procedimiento filtrosProyecto>>");
				
			}
			
			if (codigoRpta!=1) {//VERIFICACION DE ERROR EN LA BD
				log.error("[listarItems] Ocurrio un error en la operacion del Procedimiento almacenado listarItems : "+mensajeRpta);
			
			}
			
			
		} catch (Exception e) {
			log.error("DAO grabarPersona>>>>"+this.getClass().getName(), e);
			log.error("DAO grabarPersona>>>>"+ codigoRpta);
			
		}
		log.info("[DAO listarItems] FIN  ");	
		return codigoRpta;
	}

	@Override
	public ApiOutResponse listadoPersona(int idEmpresa) {
		log.info("[DAO listadoPersona] INICIO  ");	
		ApiOutResponse outResponse = new ApiOutResponse();
		List<PersonaDTO> lstPersona = new ArrayList<PersonaDTO>();
		int codigoRpta=0;
		String mensajeRpta="";
		try {
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate.getDataSource());
			jdbcCall.withSchemaName("dbo")
			.withProcedureName("listadoPersona")
			.declareParameters(
					new SqlParameter("COD_EMPRESA", Types.INTEGER),
					new SqlOutParameter("COD_RESULTADO", 	Types.VARCHAR),
					new SqlOutParameter("MSG_RESULTADO", 	Types.VARCHAR)
				);
	
			MapSqlParameterSource parametros = new MapSqlParameterSource();
			parametros.addValue("COD_EMPRESA",  Constantes.COD_EMPRESA);

			
			Map<String, Object> results = jdbcCall.execute(parametros);
			List<Map<String, Object>> rs = (List<Map<String, Object>>) results.get(Constantes.RETURN_RESULT_SET_PREFIX);
			
			codigoRpta=Integer.parseInt(results.get("COD_RESULTADO").toString());
			mensajeRpta=results.get("MSG_RESULTADO").toString();
			
			if (results.isEmpty()||results.size()<1) {
				log.info("Sin registros, verificar el procedimiento listadoPersona>>");
				return  new ApiOutResponse();
			}
			
			if (codigoRpta!=1) {//VERIFICACION DE ERROR EN LA BD
				log.error("[listarItems] Ocurrio un error en la operacion del Procedimiento almacenado listadoPersona : "+ mensajeRpta);
				outResponse.setCodResultado(codigoRpta);
				outResponse.setTotal(0);
				outResponse.setResponse("Error en la BD");
				outResponse.setMsgResultado(mensajeRpta);
				return outResponse;
			}
			
			if(rs!=null) {
				if (rs.size() > 0) {
					for (Map<String, Object> map : rs) {
						PersonaDTO personaDTO = new PersonaDTO();
						personaDTO.setCodEmpresa(Integer.parseInt(map.get("Co_empresa").toString()));
						personaDTO.setCodUsuario(Integer.parseInt(map.get("Co_Usuario").toString()));
						personaDTO.setNombreCompleto(map.get("No_Completo")!=null ? map.get("No_Completo").toString() : "");
						personaDTO.setNoUsuario(map.get("Co_Usuario")!=null ? map.get("No_Usuario").toString() : "");
						personaDTO.setCorreo(map.get("No_Correo")!=null ? map.get("No_Correo").toString() : "");
						personaDTO.setIdCargo(map.get("Co_Cargo")!=null ?Integer.parseInt(map.get("Co_Cargo").toString()): 0);
						personaDTO.setDesCargo(map.get("No_DescCargo")!=null ? map.get("No_DescCargo").toString() : "");
						personaDTO.setIdArea(map.get("Co_Area")!=null ?Integer.parseInt(map.get("Co_Area").toString()): 0);
						personaDTO.setDesArea(map.get("No_DescArea")!=null ? map.get("No_DescArea").toString() : "");			
						lstPersona.add(personaDTO);
					}
					outResponse.setResponse(lstPersona);
					outResponse.setCodResultado( results.get("COD_RESULTADO")!=null ? Integer.parseInt(results.get("COD_RESULTADO").toString()) : 500);
					outResponse.setMsgResultado( results.get("MSG_RESULTADO")!=null ? results.get("MSG_RESULTADO").toString() : "-" );
				}else {
					outResponse.setCodResultado(2);//Codigo para indicar que no existen registros en la BD
					outResponse.setTotal(0);
					outResponse.setResponse("");
					outResponse.setMsgResultado("¡No existen registros!");
				}
			}
			
		} catch (Exception e) {
			log.error("DAO listadoPersona>>>>"+this.getClass().getName(), e);
			outResponse.setCodResultado(500);
			outResponse.setMsgResultado( "Error en la Base de S!.");
		}
		log.info("[DAO listadoPersona] FIN  ");	
		return outResponse;
	}

	@Override
	public ApiOutResponse obtenerPersona(String codUsuario) {
		log.info("[DAO obtenerPersona] INICIO  ");	
		ApiOutResponse outResponse = new ApiOutResponse();
		Persona objPersona = new Persona();
		int codigoRpta=0;
		String mensajeRpta="";
		try {
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate.getDataSource());
			jdbcCall.withSchemaName("dbo")
			.withProcedureName("obtenerPersona")
			.declareParameters(
					new SqlParameter("COD_USUARIO", Types.INTEGER),
					new SqlOutParameter("COD_RESULTADO", 	Types.VARCHAR),
					new SqlOutParameter("MSG_RESULTADO", 	Types.VARCHAR)
				);
	
			MapSqlParameterSource parametros = new MapSqlParameterSource();
			parametros.addValue("COD_USUARIO",  codUsuario);

			
			Map<String, Object> results = jdbcCall.execute(parametros);
			List<Map<String, Object>> rs = (List<Map<String, Object>>) results.get(Constantes.RETURN_RESULT_SET_PREFIX);
			
			codigoRpta=Integer.parseInt(results.get("COD_RESULTADO").toString());
			mensajeRpta=results.get("MSG_RESULTADO").toString();
			
			if (results.isEmpty()||results.size()<1) {
				log.info("Sin registros, verificar el procedimiento obtenerPersona>>");
				return  new ApiOutResponse();
			}
			
			if (codigoRpta!=1) {//VERIFICACION DE ERROR EN LA BD
				log.error("[listarItems] Ocurrio un error en la operacion del Procedimiento almacenado listadoPersona : "+ mensajeRpta);
				outResponse.setCodResultado(codigoRpta);
				outResponse.setTotal(0);
				outResponse.setResponse("Error en la BD");
				outResponse.setMsgResultado(mensajeRpta);
				return outResponse;
			}
			
			if(rs!=null) {
				if (rs.size() > 0) {
					for (Map<String, Object> map : rs) {
						
						objPersona.setCodEmpresa(Integer.parseInt(map.get("Co_empresa").toString()));
						objPersona.setCodUsuario(Integer.parseInt(map.get("Co_Usuario").toString()));
						objPersona.setUsuario(map.get("No_Usuario")!=null ? map.get("No_Usuario").toString() : "");
						objPersona.setClave(map.get("No_Clave")!=null ? map.get("No_Clave").toString() : "");
						objPersona.setCodTrabajador(map.get("Co_Trabajador")!=null ? map.get("Co_Trabajador").toString() : "");
						objPersona.setPrimerNombre(map.get("No_PrimerNombre")!=null ? map.get("No_PrimerNombre").toString() : "");
						objPersona.setSegundoNombre(map.get("No_SegundoNombre")!=null ? map.get("No_SegundoNombre").toString() : "");
						objPersona.setApePaterno(map.get("No_ApePaterno")!=null ? map.get("No_ApePaterno").toString() : "");
						objPersona.setApeMaterno(map.get("No_ApeMaterno")!=null ? map.get("No_ApeMaterno").toString() : "");
						objPersona.setCorreoElectronico(map.get("No_Correo")!=null ? map.get("No_Correo").toString() : "");	
						
						objPersona.setSexo(map.get("Fi_Sexo")!=null ? map.get("Fi_Sexo").toString() : "");
						objPersona.setFecNacimiento(map.get("FecNac")!=null ? map.get("FecNac").toString() : "");
						objPersona.setFecIngreso(map.get("FecIngreso")!=null ? map.get("FecIngreso").toString() : "");
						
						objPersona.setSueldoBasico(BigDecimal.valueOf(Double.parseDouble(map.get("De_Sueldo").toString())));			
						objPersona.setIdTipoDocumento(map.get("Co_TipoDocumento")!=null ? Integer.parseInt(map.get("Co_TipoDocumento").toString()) : 0);
						objPersona.setNumDocumento(map.get("Co_NumDocumento")!=null ? map.get("Co_NumDocumento").toString() : "");
						objPersona.setIdCargo(map.get("Co_Cargo")!=null ? Integer.parseInt(map.get("Co_Cargo").toString()) : 0);
						objPersona.setIdCategoria(map.get("Co_Categoria")!=null ? Integer.parseInt(map.get("Co_Categoria").toString()) : 0);
						objPersona.setIdArea(map.get("Co_Area")!=null ? Integer.parseInt(map.get("Co_Area").toString()) : 0);
						objPersona.setIdLocal(map.get("Co_Local")!=null ? Integer.parseInt(map.get("Co_Local").toString()) : 0);
						objPersona.setIdRol(map.get("Co_Rol")!=null ? Integer.parseInt(map.get("Co_Rol").toString()) : 0);
						objPersona.setFlagEditar(1);
						
					}
					outResponse.setResponse(objPersona);
					outResponse.setCodResultado( results.get("COD_RESULTADO")!=null ? Integer.parseInt(results.get("COD_RESULTADO").toString()) : 500);
					outResponse.setMsgResultado( results.get("MSG_RESULTADO")!=null ? results.get("MSG_RESULTADO").toString() : "-" );
				}else {
					outResponse.setCodResultado(2);//Codigo para indicar que no existen registros en la BD
					outResponse.setTotal(0);
					outResponse.setResponse("");
					outResponse.setMsgResultado("¡No existen registros!");
				}
			}
			
		} catch (Exception e) {
			log.error("DAO listadoPersona>>>>"+this.getClass().getName(), e);
			outResponse.setCodResultado(500);
			outResponse.setMsgResultado( "Error en la Base de datos!.");
		}
		log.info("[DAO listadoPersona] FIN  ");	
		return outResponse;
	}

	@Override
	public int actualizarPersona(Persona persona) {
	log.info("[DAO grabarPersona] INICIO  ");	
		
		int codigoRpta=0;
		String mensajeRpta="";
		try {
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate.getDataSource());
			jdbcCall.withSchemaName("dbo")
			.withProcedureName("grabarUsuario")
			.declareParameters(
					new SqlParameter("CoUsuario", 		Types.INTEGER),
					new SqlParameter("NoUsuario", 		Types.VARCHAR),
					new SqlParameter("NoClave", 		Types.VARCHAR),
					new SqlParameter("CoTrabajador", 	Types.VARCHAR),
					new SqlParameter("NoPrimerNombre", 	Types.VARCHAR),
					new SqlParameter("NoSegundoNombre", Types.VARCHAR),
					new SqlParameter("NoApePaterno", 	Types.VARCHAR),
					new SqlParameter("NoApeMaterno", 	Types.VARCHAR),
					new SqlParameter("NoCorreo", 		Types.VARCHAR),
					new SqlParameter("FiSexo", 			Types.VARCHAR),
					new SqlParameter("FeNacimiento", 	Types.VARCHAR),
					new SqlParameter("FeIngreso", 		Types.VARCHAR),
					new SqlParameter("DeSueldo", 		Types.DECIMAL),
					new SqlParameter("CoTipoDocumento", Types.INTEGER),
					new SqlParameter("CoNumDocumento", 	Types.VARCHAR),
					new SqlParameter("CoCargo", 		Types.INTEGER),
					new SqlParameter("CoCategoria", 	Types.INTEGER),
					new SqlParameter("CoArea", 			Types.INTEGER),
					new SqlParameter("CoLocal", 		Types.INTEGER),
					new SqlParameter("CoRol", 			Types.INTEGER),
					new SqlParameter("CoEst", 			Types.INTEGER),
					new SqlParameter("flagEditar", 		Types.INTEGER),
					
					new SqlOutParameter("COD_RESULTADO",	Types.VARCHAR),
					new SqlOutParameter("MSG_RESULTADO", 	Types.VARCHAR)
				);
	
			MapSqlParameterSource parametros = new MapSqlParameterSource();
			parametros.addValue("CoUsuario", persona.getCodUsuario());
			parametros.addValue("NoUsuario", persona.getUsuario());
			parametros.addValue("NoClave", persona.getClave());
			parametros.addValue("CoTrabajador", persona.getCodTrabajador());
			parametros.addValue("NoPrimerNombre", persona.getPrimerNombre());
			parametros.addValue("NoSegundoNombre", persona.getSegundoNombre());
			parametros.addValue("NoApePaterno", persona.getApePaterno());
			parametros.addValue("NoApeMaterno", persona.getApeMaterno());
			parametros.addValue("NoCorreo", persona.getCorreoElectronico());
			parametros.addValue("FiSexo", persona.getSexo());
			parametros.addValue("FeNacimiento", persona.getFecNacimiento());
			parametros.addValue("FeIngreso", persona.getFecIngreso());
			parametros.addValue("DeSueldo", persona.getSueldoBasico());
			parametros.addValue("CoTipoDocumento", persona.getIdTipoDocumento());
			parametros.addValue("CoNumDocumento", persona.getNumDocumento());
			parametros.addValue("CoCargo", persona.getIdCargo());
			parametros.addValue("CoCategoria", persona.getIdCategoria());
			parametros.addValue("CoArea", persona.getIdArea());
			parametros.addValue("CoLocal", persona.getIdLocal());
			parametros.addValue("CoRol", persona.getIdRol());
			parametros.addValue("CoEst", persona.getIdEstado());
			parametros.addValue("flagEditar", persona.getFlagEditar());

			
			Map<String, Object> results = jdbcCall.execute(parametros);
			
			codigoRpta=Integer.parseInt(results.get("COD_RESULTADO").toString());
			mensajeRpta=results.get("MSG_RESULTADO").toString();
			
			if (results.isEmpty()||results.size()<1) {
				log.info("Sin registros, verificar el procedimiento filtrosProyecto>>");
				
			}
			
			if (codigoRpta!=1) {//VERIFICACION DE ERROR EN LA BD
				log.error("[listarItems] Ocurrio un error en la operacion del Procedimiento almacenado listarItems : "+mensajeRpta);
			
			}
			
			
		} catch (Exception e) {
			log.error("DAO actualizarPersona>>>>"+this.getClass().getName(), e);
			log.error("DAO actualizarPersona>>>>"+ codigoRpta);
			
		}
		log.info("[DAO listarItems] FIN  ");	
		return codigoRpta;
	}

	@Transactional
	@Override
	public int grabarPersonaMasivo(List<Persona> lstPersona) {
		int rpta = 0;
		for(Persona objPersona : lstPersona) {
			if(objPersona.getCodUsuario()<=0) {
			rpta = grabarPersona(objPersona);
			}else {
				rpta = actualizarPersona(objPersona);
			}
		}
		return rpta;
	}

	@Override
	public ApiOutResponse validarDataFormulario(String dato, Integer idUsuario,  String valor) {
		log.info("[DAO obtenerPersona] INICIO  ");	
		ApiOutResponse outResponse = new ApiOutResponse();
		int codigoRpta=0;
		String mensajeRpta="";
		try {
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate.getDataSource());
			jdbcCall.withSchemaName("dbo")
			.withProcedureName("validacionFormularioPersona")
			.declareParameters(
					new SqlParameter("DATO", Types.VARCHAR),
					new SqlParameter("VALOR", Types.VARCHAR),
					new SqlOutParameter("COD_RESULTADO", 	Types.VARCHAR),
					new SqlOutParameter("MSG_RESULTADO", 	Types.VARCHAR)
				);
	
			MapSqlParameterSource parametros = new MapSqlParameterSource();
			parametros.addValue("DATO",  dato);
			parametros.addValue("VALOR",  valor);
		
			Map<String, Object> results = jdbcCall.execute(parametros);
			List<Map<String, Object>> rs = (List<Map<String, Object>>) results.get(Constantes.RETURN_RESULT_SET_PREFIX);
			
			codigoRpta=Integer.parseInt(results.get("COD_RESULTADO").toString());
			mensajeRpta=results.get("MSG_RESULTADO").toString();
			
			if (results.isEmpty()||results.size()<1) {
				log.info("Sin registros, verificar el procedimiento obtenerPersona>>");
				return  new ApiOutResponse();
			}
			
			if (codigoRpta!=1) {//VERIFICACION DE ERROR EN LA BD
				log.error("[listarItems] Ocurrio un error en la operacion del Procedimiento almacenado listadoPersona : "+ mensajeRpta);
				outResponse.setCodResultado(codigoRpta);
				outResponse.setTotal(0);
				outResponse.setResponse("Error en la BD");
				outResponse.setMsgResultado(mensajeRpta);
				return outResponse;
			}
			
			if(rs!=null) {

					Object objCodUsuario = new Object();
					if(dato.equals(Constantes.VALOR_COD_TRAB) || dato.equals(Constantes.VALOR_NUM_DOCU)) {
						Map<String, Object> map = rs.get(0);
						objCodUsuario = Integer.parseInt(map.get("Co_Usuario").toString());			
					}
				
					outResponse.setCodResultado( results.get("COD_RESULTADO")!=null ? Integer.parseInt(results.get("COD_RESULTADO").toString()) : 500);
					outResponse.setMsgResultado( results.get("MSG_RESULTADO")!=null ? results.get("MSG_RESULTADO").toString() : "-" );
					
					if(idUsuario.equals(objCodUsuario)) {
						outResponse.setCodResultado(0);					
					}
				
			}
			
		} catch (Exception e) {
			log.error("DAO listadoPersona>>>>"+this.getClass().getName(), e);
			outResponse.setCodResultado(500);
			outResponse.setMsgResultado( "Error en la Base de datos!.");
		}
		log.info("[DAO listadoPersona] FIN  ");	
		return outResponse;
	}

	@Override
	public ApiOutResponse buscarPersona(String textoBuscar) {
		log.info("[DAO buscarPersona] INICIO  ");	
		ApiOutResponse outResponse = new ApiOutResponse();
		List<PersonaDTO> lstPersona = new ArrayList<PersonaDTO>();
		int codigoRpta=0;
		String mensajeRpta="";
		try {
			SimpleJdbcCall jdbcCall = new SimpleJdbcCall(jdbcTemplate.getDataSource());
			jdbcCall.withSchemaName("dbo")
			.withProcedureName("buscarPersona")
			.declareParameters(
					new SqlParameter("PRI_NOMBRE", Types.VARCHAR),
					new SqlParameter("APE_PATERNO", Types.VARCHAR),
					new SqlParameter("APE_MATERNO", Types.VARCHAR),
					new SqlOutParameter("COD_RESULTADO", 	Types.INTEGER),
					new SqlOutParameter("MSG_RESULTADO", 	Types.VARCHAR)
				);
	
			MapSqlParameterSource parametros = new MapSqlParameterSource();
			parametros.addValue("PRI_NOMBRE", textoBuscar);
			parametros.addValue("APE_PATERNO", textoBuscar);
			parametros.addValue("APE_MATERNO",  textoBuscar);

			
			Map<String, Object> results = jdbcCall.execute(parametros);
			List<Map<String, Object>> rs = (List<Map<String, Object>>) results.get(Constantes.RETURN_RESULT_SET_PREFIX);
			
			codigoRpta=Integer.parseInt(results.get("COD_RESULTADO").toString());
			mensajeRpta=results.get("MSG_RESULTADO").toString();
			
			if (results.isEmpty()||results.size()<1) {
				log.info("Sin registros, verificar el procedimiento listadoPersona>>");
				return  new ApiOutResponse();
			}
			
			if (codigoRpta!=1) {//VERIFICACION DE ERROR EN LA BD
				log.error("[listarItems] Ocurrio un error en la operacion del Procedimiento almacenado listadoPersona : "+ mensajeRpta);
				outResponse.setCodResultado(codigoRpta);
				outResponse.setTotal(0);
				outResponse.setResponse("Error en la BD");
				outResponse.setMsgResultado(mensajeRpta);
				return outResponse;
			}
			
			if(rs!=null) {
				if (rs.size() > 0) {
					for (Map<String, Object> map : rs) {
						PersonaDTO personaDTO = new PersonaDTO();
						personaDTO.setCodEmpresa(Integer.parseInt(map.get("Co_empresa").toString()));
						personaDTO.setCodUsuario(Integer.parseInt(map.get("Co_Usuario").toString()));
						personaDTO.setNombreCompleto(map.get("No_Completo")!=null ? map.get("No_Completo").toString() : "");
						personaDTO.setNoUsuario(map.get("Co_Usuario")!=null ? map.get("No_Usuario").toString() : "");
						personaDTO.setCorreo(map.get("No_Correo")!=null ? map.get("No_Correo").toString() : "");
						personaDTO.setIdCargo(map.get("Co_Cargo")!=null ?Integer.parseInt(map.get("Co_Cargo").toString()):0);
						personaDTO.setDesCargo(map.get("No_DescCargo")!=null ? map.get("No_DescCargo").toString() : "");
						personaDTO.setIdArea(map.get("Co_Area")!=null ?Integer.parseInt(map.get("Co_Area").toString()): 0);
						personaDTO.setDesArea(map.get("No_DescArea")!=null ? map.get("No_DescArea").toString() : "");			
						lstPersona.add(personaDTO);
					}
					outResponse.setResponse(lstPersona);
					outResponse.setCodResultado( results.get("COD_RESULTADO")!=null ? Integer.parseInt(results.get("COD_RESULTADO").toString()) : 500);
					outResponse.setMsgResultado( results.get("MSG_RESULTADO")!=null ? results.get("MSG_RESULTADO").toString() : "-" );
				}else {
					outResponse.setCodResultado(2);//Codigo para indicar que no existen registros en la BD
					outResponse.setTotal(0);
					outResponse.setResponse("");
					outResponse.setMsgResultado("¡No existen registros!");
				}
			}
			
		} catch (Exception e) {
			log.error("DAO listadoPersona>>>>"+this.getClass().getName(), e);
			outResponse.setCodResultado(500);
			outResponse.setMsgResultado( "Error en la Base de S!.");
		}
		log.info("[DAO listadoPersona] FIN  ");	
		return outResponse;
	}

	

}

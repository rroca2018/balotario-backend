package com.balotario.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import com.balotario.bean.PersonaExcelAux;


public class ArrayRepetido {

	public static void main(String[] args) {
	    int[] ar = { 1, 2, 2, 1, 1, 3, 5, 1, 2 };
	    String[] array = {"TBJ-020","TBJ-022","TBJ-023","TBJ-019","TBJ-026","TBJ-028","TBJ-021"};
	    
		PersonaExcelAux objPersona1 =  new PersonaExcelAux();
		PersonaExcelAux objPersona2 =  new PersonaExcelAux();
		PersonaExcelAux objPersona3 =  new PersonaExcelAux();
		PersonaExcelAux objPersona4 =  new PersonaExcelAux();
		PersonaExcelAux objPersona5 =  new PersonaExcelAux();
		PersonaExcelAux objPersona6 =  new PersonaExcelAux();
		PersonaExcelAux objPersona7 =  new PersonaExcelAux();
		
		objPersona1.setCodTrabajador("TBJ-020");
		objPersona1.setColumnaExcel("T");
		objPersona1.setCodTrabajador("TBJ-027");
		objPersona2.setCodTrabajador("TBJ-022");
		objPersona3.setCodTrabajador("TBJ-020");
		objPersona4.setCodTrabajador("TBJ-023");
		objPersona5.setCodTrabajador("TBJ-019");
		objPersona6.setCodTrabajador("TBJ-026");
		objPersona7.setCodTrabajador("TBJ-023");
		
		objPersona1.setNumDocumento("11111111");
		objPersona2.setNumDocumento("22222222");
		objPersona3.setNumDocumento("33333333");
		objPersona4.setNumDocumento("44444444");
		objPersona5.setNumDocumento("66666666");
		objPersona6.setNumDocumento("55555555");
		objPersona7.setNumDocumento("22222222");
		
		ArrayList<PersonaExcelAux> lstPersona = new ArrayList<>();
		lstPersona.add(objPersona1); 
		lstPersona.add(objPersona2);
		lstPersona.add(objPersona3);
		lstPersona.add(objPersona4);
		lstPersona.add(objPersona5);
		lstPersona.add(objPersona6);
		lstPersona.add(objPersona7);
	    
	    HashMap<String, Object> repetido = encontrarRepetidos(lstPersona);
	    
	    if(repetido.get("trabajadorRepetido")!=null) {
	    	  PersonaExcelAux objPersonaExcelAux =    (PersonaExcelAux) repetido.get("trabajadorRepetido");
	    	  System.out.println( "VALOR : " + objPersonaExcelAux.getCodTrabajador()  + " - " +
	    			     objPersonaExcelAux.getColumnaExcel() + " - " + objPersonaExcelAux.getFilaExcel());
	    			    System.out.println( "VALOR : " + repetido.get("docuemntoRepetido"));
	    }else {
	    	System.out.println("No se encontro repetidos en los trabajdores");
	    }
	  
	    
	   
	    /*
	    // ARRAY ORDENADO
	    Arrays.sort(ar);

	    // ARRAY DE NUMERO DE ELEMENTOS
	    int[] arNumElementos = encontrarElementosRepetidos(ar);

	    // ARRAY DE NUMEROS NO REPETIDOS ORDENADOS
	    int[] arNoDuplicates = eliminarDuplicados(ar);

	    // CONTAR NUMERO DE ELEMENTOS PARA EL ARRAY BIDIMENSIONAL
	    int contadorOnly = 0;
	    for (int elemento : arNoDuplicates) {
	        if (elemento != 0)
	            contadorOnly++;
	    }

	    // ARRAY BIDIMENSIONAL CON EL ELEMENTO Y NUMERO DE ELEMENTOS
	    int[][] arRepetidos = new int[contadorOnly][2];

	    for (int x = 0; x < arRepetidos.length; x++)
	        for (int y = 0; y < arRepetidos[x].length; y++)
	            if (y == 0)
	                arRepetidos[x][y] = arNoDuplicates[x];
	            else
	                arRepetidos[x][y] = arNumElementos[x];

	    // IMPRIMIR ARRAY BIDIMENSIONAL
	    for (int x = 0; x < arRepetidos.length; x++) {
	        System.out.print("{ ");
	        for (int y = 0; y < arRepetidos[x].length; y++)
	            System.out.print("" + arRepetidos[x][y] + " ");
	        System.out.print("},");
	    }*/

	}
/*
	private static int[] eliminarDuplicados(int[] arOriginal) {
	    int[] tempArray = new int[arOriginal.length];

	    int indexJ = 0;
	    for (int i = 0; i < arOriginal.length - 1; i++) {
	        int elemento = arOriginal[i];
	        if (elemento != arOriginal[i + 1]) {
	            tempArray[indexJ++] = elemento;
	        }
	    }

	    tempArray[indexJ++] = arOriginal[arOriginal.length - 1];

	    return tempArray;
	}

	private static int[] encontrarElementosRepetidos(int[] arOriginal) {
	    // ARRAY ORDENADO
	    Arrays.sort(arOriginal);

	    // ARRAY DE NUMERO DE ELEMENTOS
	    int[] arNumElementos = new int[arOriginal.length];
	    int contador = 0;
	    int aux = arOriginal[0];
	    int iterador = 0;
	    for (int i = 0; i < arOriginal.length; i++) {
	        if (aux == arOriginal[i]) {
	            contador++;
	        } else {
	            arNumElementos[iterador] = contador;
	            contador = 1;
	            aux = arOriginal[i];
	            iterador++;
	        }

	    }
	    arNumElementos[iterador] = contador;
	    return arNumElementos;
	}*/
	
	
	private static HashMap<String, Object> encontrarRepetidos(ArrayList<PersonaExcelAux> lstPersona) {
		
		HashMap<String, Object> resultado = new HashMap<String, Object>();
		outerloop:
		for (int i=0;i<lstPersona.size();i++)
		{
			for(int j=0;j<lstPersona.size();j++) {
				if(i != j) {
					if ( lstPersona.get(i).getCodTrabajador().equals(lstPersona.get(j).getCodTrabajador())){	
						resultado.put("trabajadorRepetido",  lstPersona.get(i));
						System.out.println("repetido trabajador: " +  lstPersona.get(i).getCodTrabajador());
						break outerloop;
						
					}
					if(lstPersona.get(i).getNumDocumento().equals(lstPersona.get(j).getNumDocumento())) {
						resultado.put("documentoRepetido", lstPersona.get(i));
						System.out.println("repetido documento: " +  lstPersona.get(i).getNumDocumento());
						break outerloop;
					}
				}			
			}
	
		}
		
		return resultado;
		
	}
	

}

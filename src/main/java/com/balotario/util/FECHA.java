package com.balotario.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;


import static org.apache.poi.ss.usermodel.Row.MissingCellPolicy;

public class FECHA {

		public static final String EXCEL_FILE = "D:/plantilla-excel/Plantilla_Carga_Usuarios.xls";

		public static void readXLSFile() {
			try {
				InputStream ExcelFileToRead = new FileInputStream(EXCEL_FILE);
				HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);

				HSSFSheet sheet = wb.getSheetAt(0);
				HSSFRow row;
				HSSFCell cell;

				Iterator rows = sheet.rowIterator();
				
				while (rows.hasNext()) {
					row = (HSSFRow) rows.next();
					
					System.out.println(row.getRowNum());
					System.out.println(row.cellIterator());
					
					
					Iterator cells = row.cellIterator();
					System.out.println(cells);
					System.out.println(cells.hasNext());
					while (cells.hasNext()) {
						cell = (HSSFCell) cells.next();
						System.out.print(cell.toString()+" ");
					}
					System.out.println();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		}
		
		public static void readXLSFileWithBlankCells() {
			try {
				InputStream ExcelFileToRead = new FileInputStream(EXCEL_FILE);
				HSSFWorkbook wb = new HSSFWorkbook(ExcelFileToRead);

				HSSFSheet sheet = wb.getSheetAt(0);
				HSSFRow row;
				HSSFCell cell;

				Iterator rows = sheet.rowIterator();

				while (rows.hasNext()) {
					row = (HSSFRow) rows.next();
					
					for(int i=0; i<row.getLastCellNum(); i++) {
						cell = row.getCell(i, Row.MissingCellPolicy.CREATE_NULL_AS_BLANK);
						System.out.print(cell.toString()+" ");
					}
					System.out.println();
				}

			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		public static void main(String[] args) {
			//readXLSFile();
			readXLSFileWithBlankCells();
		}


}

/*
 * 
 */
package com.balotario.util;

public class Constantes {

	/* DATOS GENERALES */
	public static final String ETIQUETA_SECRET	 		= "password";
	public static final String DIRECTORIO_UPLOAD 		= "uploads";
	public static final String MENSAJE 					= "mensaje";
	public static final String ERROR 					= "error";
	public static final String ERRORES 					= "errores";
	public static final String CLIENTE 					= "cliente";
	public static final String RETURN_RESULT_SET_PREFIX = "#result-set-1"; 
	public static final Integer COD_EMPRESA = 1;
	
	public static final String EMPRESA = "EMPRESA";
	public static final String USUARIO = "USUARIO";
	public static final String TIPO_DOCUMENTO_EXCEL = "TIPODOCUMENTO";




	//LISTADO DE COMBOS SEGÚN EL TIPO DE PARAMETRO
	public static final String TIPO_DOCUMENTO = "TIPO_DOCUMENTO"; 
	public static final String CARGO = "CARGO"; 
	public static final String CATEGORIA = "CATEGORIA";
	public static final String AREA = "AREA"; 
	public static final String LOCAL = "LOCAL"; 
	public static final String ROL = "ROL"; 
	public static final String ESTADO = "ESTADO";

	
	
	public static final String EXTENSION_FORMATO_PDF = ".pdf";
	public static final String EXTENSION_FORMATO_XLS = ".xls";
	public static final String EXTENSION_FORMATO_XLSX = ".xlsx";
	public static final String EXTENSION_FORMATO_DOC = ".doc";
	public static final String EXTENSION_FORMATO_DOCX = ".docx";
	public static final String EMPTY = "";
	public static final short ZERO_SHORT = 0;
	public static final int ZERO_INT = 0;
	public static final long ZERO_LONG = 0;
	public static final String ZERO_STRING = "0";
	
	/* CODIGOS PARA MANEJO DE CADENAS */
	public static final String CADENA_VACIO = "";
	public static final String COD_CADENA_CORTADA = "...(*)";
	public static final String PUNTO = ".";
	public static final String DOS_PUNTOS = ":";
	public static final String SEPARADOR = "-";
	public static final String DIVISOR = "/";
	public static final String PORCENTAJE = "%";
	public static final String EXPRESION_OR = "||";
	public static final String EXPRESION_AND = "&&";
	public static final String UNDERLINE = "_";
	public static final String PIPELINE = "|";
	public static final String SALTO_LINEA = "\r\n";
	public static final String SALTO_LINEA_PARRAFO = "\n";
	public static final String IMAGEN_NO_DISPONIBLE	= "no-disponible.png";
	
	/* CODIGOS PARA MESES */
	public static final short ENERO = 1;
	public static final short FEBRERO = 2;
	public static final short MARZO = 3;
	public static final short ABRIL = 4;
	public static final short MAYO = 5;
	public static final short JUNIO = 6;
	public static final short JULIO = 7;
	public static final short AGOSTO = 8;
	public static final short SEPTIEMBRE = 9;
	public static final short OCTUBRE = 10;
	public static final short NOVIEMBRE = 11;
	public static final short DICIEMBRE = 12;
	/* MEDIA TYPE */
	public static final String MIME_APPLICATION_DOCX = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
	public static final String MIME_APPLICATION_DOC = "application/msword";
	public static final String MIME_APPLICATION_PDF = "application/pdf";
	public static final String MIME_APPLICATION_XLS = "application/vnd.ms-excel";
	public static final String MIME_APPLICATION_XLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
	public static final String MIME_APPLICATION_TXT = "text/plain";
	public static final String MIME_IMAGE_JPG = "image/jpeg";
	public static final String MIME_IMAGE_PNG = "image/png";
	public static final String MIME_IMAGE_GIF = "image/gif";
	/* CODIGO DE ERRORES */
    public static final String COD_VALIDACION_GENERAL = "02";
	public static final String COD_EXITO_GENERAL = "01";
	public static final String COD_ERROR_GENERAL = "00";
	
	/* DEFINICION DE LA RUTA DEL REPOSITROIO DE ARCHIVO */
	public static final String RUTA_ARCHIVO = "D:\\prefactibilidad\\";

	
	/* MENSAJES DE CONTROL DE ERRORES EXCEL*/
	public static final String EXCEL_RESPUESTA_CERO  = "0";
	public static final String EXCEL_EMPRESA_NO_EXISTE  = "Empresa no existe";
	public static final String EXCEL_EMPRESA_OBLIGATORIA = "El nombre de la empresa es obligatoria";
	public static final String EXCEL_USUARIO_OBLIGATORIO = "El usuario es obligatorio";
	public static final String EXCEL_CLAVE_OBLIGATORIA = "La clave es obligatoria";
	public static final String EXCEL_CLAVE_MINIMO_OCHO_CARACTERES = "La clave debe tener como mínimo 8 caracteres";
	public static final String EXCEL_CODIGO_TRABAJADOR_OBLIGATORIO = "Código de trabajador es obligatorio";
	public static final String EXCEL_CODIGO_TRABAJADOR_EXISTE = "Código de trabajador se repite o ya existe";
	public static final String EXCEL_CODIGO_TRABAJADOR_REPETIDO = "Código de trabajador se repite";
	public static final String EXCEL_PRIMER_NOMBRE_OBLIGATORIO = "Primer nombre del trabajador es obligatoria";
	public static final String EXCEL_APE_PATERNO_OBLIGATORIO = "Apellido paterno del trabajador es obligatoria";
	public static final String EXCEL_APE_MATERNO_OBLIGATORIO = "Apellido materno del trabajador es obligatoria";
	public static final String EXCEL_CORREO_OBLIGATORIO = "El correo del trabajador es obligatoria";
	public static final String EXCEL_CORREO_FORMATO_INVALIDO = "El correo debe contener @";
	public static final String EXCEL_SEXO_NO_CORRECTO = "Valor para Sexo es incorrecto";
	public static final String EXCEL_SEXO_OBLIGATORIO = "El sexo es obligatorio";
	public static final String EXCEL_FECHA_NACIMIENTO_OBLIGATORIO = "Fecha de nacimiento del trabajador es obligatoria";
	public static final String EXCEL_FECHA_NACIMIENTO_INVALIDA = "Fecha de nacimiento es invalida";
	public static final String EXCEL_FECHA_INGRESO_OBLIGATORIO = "Fecha de ingreso del trabajador es obligatoria";
	public static final String EXCEL_FECHA_INGRESO_MAYOR_FECHA_NACIMIENTO = "Fecha de ingreso del trabajador no puede ser menor a fecha de nacimiento";
	public static final String EXCEL_SUELDO_NEGATIVO = "Sueldo no puede ser negativo";
	public static final String EXCEL_TIPO_DOCUMENTO_OBLIGATORIO = "Tipo de documento obligatorio";
	public static final String EXCEL_TIPO_DOCUMENTO_NO_EXISTE = "Tipo de documento no existe";
	public static final String EXCEL_NUMERO_DOCUMENTO_OBLIGATORIO = "Nro. de documento del trabajador es obligatoria";
	public static final String EXCEL_NUMERO_DOCUMENTO_EXISTE = "El número de documento ya existe";
	public static final String EXCEL_NUMERO_DOCUMENTO_REPETIDO = "El número de documento se repite";
	public static final String EXCEL_CARGO_NO_EXISTE = "Cargo no existe";
	public static final String EXCEL_CATEGORIA_NO_EXISTE = "Categoria no existe";
	public static final String EXCEL_AREA_NO_EXISTE = "Área no existe";
	public static final String EXCEL_LOCAL_NO_EXISTE = "Local no existe";
	public static final String EXCEL_ROL_NO_EXISTE = "Rol no existe";
	public static final String EXCEL_ESTADO_NO_EXISTE = "Estado no existe";


	
	
	/* VALIDACION FORMULARIO */
	public static final String VALOR_USUARIO  = "USUARIO";
	public static final String VALOR_COD_TRAB  = "CODTRABAJADOR";
	public static final String VALOR_NUM_DOCU  = "NUMDOCUMENTO";
	
}
package com.balotario.bean;

import java.util.ArrayList;
import java.util.HashMap;

public class PersonaExcelAux {
	
	private String codTrabajador;
	private String NumDocumento;
	private int filaExcel;
	private String columnaExcel;
	
	
	public String getCodTrabajador() {
		return codTrabajador;
	}
	public void setCodTrabajador(String codTrabajador) {
		this.codTrabajador = codTrabajador;
	}
	public String getNumDocumento() {
		return NumDocumento;
	}
	public void setNumDocumento(String numDocumento) {
		NumDocumento = numDocumento;
	}
	public int getFilaExcel() {
		return filaExcel;
	}
	public void setFilaExcel(int filaExcel) {
		this.filaExcel = filaExcel;
	}
	public String getColumnaExcel() {
		return columnaExcel;
	}
	public void setColumnaExcel(String columnaExcel) {
		this.columnaExcel = columnaExcel;
	}
	
	
	
	
	
	
}

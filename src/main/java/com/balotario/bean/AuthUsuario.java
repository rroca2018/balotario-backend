package com.balotario.bean;

public class AuthUsuario {

	private long idCodigo;
	private String cidUsuario;
	private String cidClave;
	private String cidNombre;
	private String puesto;

	private long idCargo;
	private long idBalotario;
	private int cantidadIntento;
	private String cantidadTiempo;
	private int cantidadPregunta;

	public long getIdCargo() {
		return idCargo;
	}

	public void setIdCargo(long idCargo) {
		this.idCargo = idCargo;
	}

	public long getIdBalotario() {
		return idBalotario;
	}

	public void setIdBalotario(long idBalotario) {
		this.idBalotario = idBalotario;
	}

	public int getCantidadIntento() {
		return cantidadIntento;
	}

	public void setCantidadIntento(int cantidadIntento) {
		this.cantidadIntento = cantidadIntento;
	}

	public String getCantidadTiempo() {
		return cantidadTiempo;
	}

	public void setCantidadTiempo(String cantidadTiempo) {
		this.cantidadTiempo = cantidadTiempo;
	}

	public int getCantidadPregunta() {
		return cantidadPregunta;
	}

	public void setCantidadPregunta(int cantidadPregunta) {
		this.cantidadPregunta = cantidadPregunta;
	}

	public long getIdCodigo() {
		return idCodigo;
	}

	public void setIdCodigo(long idCodigo) {
		this.idCodigo = idCodigo;
	}

	public String getCidUsuario() {
		return cidUsuario;
	}

	public void setCidUsuario(String cidUsuario) {
		this.cidUsuario = cidUsuario;
	}

	public String getCidClave() {
		return cidClave;
	}

	public void setCidClave(String cidClave) {
		this.cidClave = cidClave;
	}

	public String getCidNombre() {
		return cidNombre;
	}

	public void setCidNombre(String cidNombre) {
		this.cidNombre = cidNombre;
	}

	public String getPuesto() {
		return puesto;
	}

	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}

}

package com.balotario.bean;

public class Item {

	private int idCodigo;
	private String cidNombre;
	private int idCategoria;
	
	public int getIdCodigo() {
		return idCodigo;
	}
	public void setIdCodigo(int idCodigo) {
		this.idCodigo = idCodigo;
	}
	public String getCidNombre() {
		return cidNombre;
	}
	public void setCidNombre(String cidNombre) {
		this.cidNombre = cidNombre;
	}
	public int getIdCategoria() {
		return idCategoria;
	}
	
	public void setIdCategoria(int idCtegoria) {
		this.idCategoria = idCtegoria;
	}
	
	
}

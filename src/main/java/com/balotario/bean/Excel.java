package com.balotario.bean;

public class Excel {
	
	private String message;
	private int nroFila;
	private String nroColumna;
	
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getNroFila() {
		return nroFila;
	}
	public void setNroFila(int nroFila) {
		this.nroFila = nroFila;
	}
	public String getNroColumna() {
		return nroColumna;
	}
	public void setNroColumna(String nroColumna) {
		this.nroColumna = nroColumna;
	}
	
	
	

}

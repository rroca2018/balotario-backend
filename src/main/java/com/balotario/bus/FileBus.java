package com.balotario.bus;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;

import com.balotario.api.ApiOutResponse;


public interface FileBus {

	ApiOutResponse validarCargaExcelPersona( Long idUsuario, MultipartFile  file, HttpServletRequest request);
	byte[] verTxtErrores(String nombreListado, HttpServletRequest request );
	ApiOutResponse buscarDato(String dato, String valor);
}

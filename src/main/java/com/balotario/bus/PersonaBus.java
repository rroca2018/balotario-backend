package com.balotario.bus;

import com.balotario.api.ApiOutResponse;
import com.balotario.entity.Persona;

public interface PersonaBus {

	ApiOutResponse listarTipoDocumento();
	ApiOutResponse listarCategoria();
	ApiOutResponse listarCargo();
	ApiOutResponse listarRol();
	ApiOutResponse listarArea(int idCategoria);
	ApiOutResponse listarLocal(int idCategoria, int idArea);
	int registrarUsuario(Persona persona);
	ApiOutResponse listadoPersona(int idEmpresa);
	
	ApiOutResponse obtenerPersona(String codUsuario);
	
	int actualizarPersona(Persona persona);
	
	ApiOutResponse usuarioExiste(String valor);
	ApiOutResponse codTrabajadorExiste(Integer idUsuario, String valor);
	ApiOutResponse numDocumentoExiste(Integer idUsuario, String valor);
	
	ApiOutResponse buscarPersona(String textoBuscar);
}

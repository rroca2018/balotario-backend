/*
 * 
 */
package com.balotario.bus;

import java.util.List;

import com.balotario.bean.AuthUsuario;
import com.balotario.bean.ColeccionMenu;

public interface AccesoBus {

	public AuthUsuario findByUsername(String username);
	
	public List<ColeccionMenu> menu(int idUsuario);

}

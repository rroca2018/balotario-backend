package com.balotario.bus;

import com.balotario.api.ApiOutResponse;

public interface CuestionarioBus {
	
	ApiOutResponse listaPregunta(Long cantidadPregunta,Long idBalotario);

}

package com.balotario.entity;

import java.io.Serializable;

public class MensajeError implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private int id;
     private String campo;
     private String descripcion;
     
	

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCampo() {
		return campo;
	}
	public void setCampo(String campo) {
		this.campo = campo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
     

}

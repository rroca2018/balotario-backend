package com.balotario.entity;

public class Respuesta {

	private Long idRespuesta;
	private String nombreRespuesta;

	public Long getIdRespuesta() {
		return idRespuesta;
	}

	public void setIdRespuesta(Long idRespuesta) {
		this.idRespuesta = idRespuesta;
	}

	public String getNombreRespuesta() {
		return nombreRespuesta;
	}

	public void setNombreRespuesta(String nombreRespuesta) {
		this.nombreRespuesta = nombreRespuesta;
	}

}

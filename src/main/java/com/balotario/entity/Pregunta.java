package com.balotario.entity;

import java.util.List;

public class Pregunta {

	private Long idPregunta;
	private String nombrePregunta;
	private Long idRespuestaCorrecto;
	private List<Respuesta> respuesta;

	public Long getIdPregunta() {
		return idPregunta;
	}

	public void setIdPregunta(Long idPregunta) {
		this.idPregunta = idPregunta;
	}

	public String getNombrePregunta() {
		return nombrePregunta;
	}

	public void setNombrePregunta(String nombrePregunta) {
		this.nombrePregunta = nombrePregunta;
	}

	public Long getIdRespuestaCorrecto() {
		return idRespuestaCorrecto;
	}

	public void setIdRespuestaCorrecto(Long idRespuestaCorrecto) {
		this.idRespuestaCorrecto = idRespuestaCorrecto;
	}

	public List<Respuesta> getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(List<Respuesta> respuesta) {
		this.respuesta = respuesta;
	}

}
